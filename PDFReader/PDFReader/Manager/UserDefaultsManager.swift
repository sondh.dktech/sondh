//
//  UserDefaultsManager.swift
//  Wallpaper
//
//  Created by DEV on 26/07/2023.
//

import Foundation
import Photos
import UIKit

class UserDefaultsManager {
    static let shared = UserDefaultsManager()
    
    private let defaults = UserDefaults.standard
    
    private init() {}
    
    private struct Keys {
        static let isFirstLoadLanguage = "IsFirstLoadLanguage"
        static let isFirstOpenApp = "IsFirstOpenAppKey"
        static let language = "LanguageKey"
        static let pdfData = "pdfData"
        static let isFirstOpenCMP = "isFirstOpenCMP"
        static let countShowInterAds = "CountShowInterAdsKey"
        static let adsEnabled = "AdsEnableKey"
        static let isFirstOpenChooseLanguage = "IsFirstOpenChooseLanguage"
        static let isShowPermissionNoti = "IsShowPermissionNoti"
        static let isRatingStar = "isRatingStar"
        static let isShowRatingStar = "isShowRatingStar"
        static let isRequestRate = "isRequestRate"
        static let countShowRating = "CountShowRating"
        static let isOpenSettings = "IsOpenSettings"
        static let isShowInter = "IsShowInter"
        static let isAccessCamera = "isAccessCamera"
        static let isShowSplashInter = "isShowSplashInter"
        static let isSplash = "isSplash"
        static let checkCMP = "checkCMP"
        static let isOpenCamera = "isOpenCamera"
        static let isOpenLibrary = "isOpenLibrary"
        static let backgroundShare = "backgroundShare"
        static let completeShare = "completeShare"
        static let isFullAccess = "isFullAccess"
        static let isNotiChoose = "isNotiChoose"
        static let currentPDF = "currentPDF"
        static let isOpenNoti = "isOpenNoti"
        static let top50Threshold = "top50Threshold"
        static let top40Threshold = "top40Threshold"
        static let top30Threshold = "top30Threshold"
        static let top20Threshold = "top20Threshold"
        static let top10Threshold = "top10Threshold"
        static let tCPAOnedayAdRevenueCache = "tCPAOnedayAdRevenueCache"
        static let isLocationUS = "isLocationUS"
    }
    
    let languages: [Languages] = [
        Languages(language: Language.hindi, code: false),
        Languages(language: Language.spanish, code: false),
        Languages(language: Language.french, code: false),
        Languages(language: Language.russian, code: false),
        Languages(language: Language.portuguese, code: false),
        Languages(language: Language.indonesian, code: false),
        Languages(language: Language.german, code: false),
        Languages(language: Language.italian, code: false),
        Languages(language: Language.korean, code: false),
        Languages(language: Language.arabic, code: false),
        Languages(language: Language.english, code: true),
    ]
    
    let pdfDatas: [PDFData] = []
    
    var isLocationUS: Bool {
        get {
            return defaults.bool(forKey: Keys.isLocationUS)
        }
        set {
            defaults.set(newValue, forKey: Keys.isLocationUS)
        }
    }
    
    var tCPAOnedayAdRevenueCache: Double {
        get {
            return defaults.double(forKey: Keys.tCPAOnedayAdRevenueCache)
        }
        set {
            defaults.set(newValue, forKey: Keys.tCPAOnedayAdRevenueCache)
        }
    }
    
    var top10Threshold: Double {
        get {
            return defaults.double(forKey: Keys.top10Threshold)
        }
        set {
            defaults.set(newValue, forKey: Keys.top10Threshold)
        }
    }
    
    var top20Threshold: Double {
        get {
            return defaults.double(forKey: Keys.top20Threshold)
        }
        set {
            defaults.set(newValue, forKey: Keys.top20Threshold)
        }
    }
    
    var top30Threshold: Double {
        get {
            return defaults.double(forKey: Keys.top30Threshold)
        }
        set {
            defaults.set(newValue, forKey: Keys.top30Threshold)
        }
    }
    
    var top40Threshold: Double {
        get {
            return defaults.double(forKey: Keys.top40Threshold)
        }
        set {
            defaults.set(newValue, forKey: Keys.top40Threshold)
        }
    }
    
    var top50Threshold: Double {
        get {
            return defaults.double(forKey: Keys.top50Threshold)
        }
        set {
            defaults.set(newValue, forKey: Keys.top50Threshold)
        }
    }
    
    var currentPDF: String {
        get {
            return defaults.string(forKey: Keys.currentPDF) ?? ""
        }
        set {
            defaults.set(newValue, forKey: Keys.currentPDF)
        }
    }
    
    var isOpenNoti: Bool {
        get {
            return defaults.bool(forKey: Keys.isOpenNoti)
        }
        set {
            defaults.set(newValue, forKey: Keys.isOpenNoti)
        }
    }
    
    var isNotiChoose: Bool {
        get {
            return defaults.bool(forKey: Keys.isNotiChoose)
        }
        set {
            defaults.set(newValue, forKey: Keys.isNotiChoose)
        }
    }
    
    var isFullAccess: Bool {
        get {
            return defaults.bool(forKey: Keys.isFullAccess)
        }
        set {
            defaults.set(newValue, forKey: Keys.isFullAccess)
        }
    }
    
    var completeShare: Bool {
        get {
            return defaults.bool(forKey: Keys.completeShare)
        }
        set {
            defaults.set(newValue, forKey: Keys.completeShare)
        }
    }
    
    var backgroundShare: Bool {
        get {
            return defaults.bool(forKey: Keys.backgroundShare)
        }
        set {
            defaults.set(newValue, forKey: Keys.backgroundShare)
        }
    }
    
    var isOpenCamera: Bool {
        get {
            return defaults.bool(forKey: Keys.isOpenCamera)
        }
        set {
            defaults.set(newValue, forKey: Keys.isOpenCamera)
        }
    }
    
    var isOpenLibrary: Bool {
        get {
            return defaults.bool(forKey: Keys.isOpenLibrary)
        }
        set {
            defaults.set(newValue, forKey: Keys.isOpenLibrary)
        }
    }
    
    var checkCMP: Bool {
        get {
            return defaults.bool(forKey: Keys.checkCMP)
        }
        set {
            defaults.set(newValue, forKey: Keys.checkCMP)
        }
    }
    
    var isFirstLoadLanguage: Bool {
        get {
            return defaults.bool(forKey: Keys.isFirstLoadLanguage)
        }
        set {
            defaults.set(newValue, forKey: Keys.isFirstLoadLanguage)
        }
    }
    
    var isOpenSettings: Bool {
        get {
            return defaults.bool(forKey: Keys.isOpenSettings)
        }
        set {
            defaults.set(newValue, forKey: Keys.isOpenSettings)
        }
    }
    
    var isFirstOpenApp: Bool {
        get {
            return defaults.bool(forKey: Keys.isFirstOpenApp)
        }
        set {
            defaults.set(newValue, forKey: Keys.isFirstOpenApp)
        }
    }
    
    var isAccessCamera: Bool {
        get {
            return defaults.bool(forKey: Keys.isAccessCamera)
        }
        set {
            defaults.set(newValue, forKey: Keys.isAccessCamera)
        }
    }
    
    var isFirstOpenChooseLanguage: Bool {
        get {
            return defaults.bool(forKey: Keys.isFirstOpenChooseLanguage)
        }
        set {
            defaults.set(newValue, forKey: Keys.isFirstOpenChooseLanguage)
        }
    }
    
    var isSplash: Bool {
        get {
            return defaults.bool(forKey: Keys.isSplash)
        }
        set {
            defaults.set(newValue, forKey: Keys.isSplash)
        }
    }
    
    var isShowSplashInter: Bool {
        get {
            return defaults.bool(forKey: Keys.isShowSplashInter)
        }
        set {
            defaults.set(newValue, forKey: Keys.isShowSplashInter)
        }
    }
    
    var isShowPermissionNoti: Bool {
        get {
            return defaults.bool(forKey: Keys.isShowPermissionNoti)
        }
        set {
            defaults.set(newValue, forKey: Keys.isShowPermissionNoti)
        }
    }
    
    var isFirstOpenCMP: Bool {
        get {
            return defaults.bool(forKey: Keys.isFirstOpenCMP)
        }
        set {
            defaults.set(newValue, forKey: Keys.isFirstOpenCMP)
        }
    }
    
    var isRequestRate: Bool {
        get {
            return defaults.bool(forKey: Keys.isRequestRate)
        }
        set {
            defaults.set(newValue, forKey: Keys.isRequestRate)
        }
    }
    
    var isShowRatingStar: Bool {
        get {
            return defaults.bool(forKey: Keys.isShowRatingStar)
        }
        set {
            defaults.set(newValue, forKey: Keys.isShowRatingStar)
        }
    }
    
    var isRatingStar: Bool {
        get {
            return defaults.bool(forKey: Keys.isRatingStar)
        }
        set {
            defaults.set(newValue, forKey: Keys.isRatingStar)
        }
    }
    
    var countShowRating: Int {
        get {
            return defaults.integer(forKey: Keys.countShowRating)
        }
        set {
            defaults.set(newValue, forKey: Keys.countShowRating)
        }
    }
    
    var countShowInterAds: Int {
        get {
            return defaults.integer(forKey: Keys.countShowInterAds)
        }
        set {
            defaults.set(newValue, forKey: Keys.countShowInterAds)
        }
    }
    
    var adsEnable: Int {
        get {
            return defaults.integer(forKey: Keys.adsEnabled)
        }
        set {
            defaults.set(newValue, forKey: Keys.adsEnabled)
        }
    }
    
    var isShowInter: Bool {
        get {
            return defaults.bool(forKey: Keys.isShowInter)
        }
        set {
            defaults.set(newValue, forKey: Keys.isShowInter)
        }
    }
    
    
    func removeRequestRate() {
        defaults.removeObject(forKey: Keys.isRequestRate)
    }
    
    func removeCountShowInterAds() {
        defaults.removeObject(forKey: Keys.countShowInterAds)
    }
    
    func removeCountShowRating() {
        defaults.removeObject(forKey: Keys.countShowRating)
    }
    
    func removeIsShowInter() {
        defaults.removeObject(forKey: Keys.isShowInter)
    }
    
    func removeIsShowRatingStar() {
        defaults.removeObject(forKey: Keys.isShowRatingStar)
    }
    
    func removeAllValues() {
        let domain = Bundle.main.bundleIdentifier!
        defaults.removePersistentDomain(forName: domain)
        defaults.synchronize()
    }
    
    func encodeArrayToData<T: Codable>(_ array: [T]) -> Data? {
        do {
            let encoder = JSONEncoder()
            return try encoder.encode(array)
        } catch {
            print("Error encoding array to data: \(error)")
            return nil
        }
    }
    
    func saveArrayToUserDefaults<T: Codable>(_ array: [T], forKey key: String) {
        guard let data = encodeArrayToData(array) else { return }
        UserDefaults.standard.set(data, forKey: key)
    }
    
    func decodeJSONFromFile<T: Codable>(filename: String, fileExtension: String, type: T.Type) -> T? {
        guard let filePath = Bundle.main.path(forResource: filename, ofType: fileExtension) else {
            print("JSON file '\(filename).\(fileExtension)' not found.")
            return nil
        }
        
        do {
            let fileURL = URL(fileURLWithPath: filePath)
            let data = try Data(contentsOf: fileURL)
            
            let decoder = JSONDecoder()
            let decodedData = try decoder.decode(T.self, from: data)
            return decodedData
        } catch {
            print("Error decoding JSON from file:", error)
            return nil
        }
    }
    
    func retrieveArrayFromUserDefaults<T: Codable>(forKey key: String) -> [T]? {
        guard let data = UserDefaults.standard.data(forKey: key) else {
            
            return nil }
        do {
            let decoder = JSONDecoder()
            return try decoder.decode([T].self, from: data)
        } catch {
            print("Error decoding data to array: \(error)")
            return nil
        }
    }
    
    func isiPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    func firstSaveLanguages() {
        saveArrayToUserDefaults(languages, forKey: Keys.language)
    }
    
    func saveLanguage(listLanguages: [Languages]) {
        saveArrayToUserDefaults(listLanguages, forKey: Keys.language)
    }
    
    func setLanguageDefaults() -> (Language,Int) {
        if let retrievedLanguages: [Languages] = retrieveArrayFromUserDefaults(forKey: Keys.language) {
            for (idx,language) in retrievedLanguages.enumerated() {
                if language.code {
                    return (language.language,idx)
                }
            }
        }
        return (.english,0)
    }
    
    func getLanguage() -> [Languages]{
        if let retrievedLanguages: [Languages] = retrieveArrayFromUserDefaults(forKey: Keys.language) {
            return retrievedLanguages
        }
        return []
    }
    
    func firstLoadData() {
        saveArrayToUserDefaults(pdfDatas, forKey: Keys.pdfData)
    }
    
    func savePDF(listPDF: [PDFData]) {
        saveArrayToUserDefaults(listPDF, forKey: Keys.pdfData)
    }

    func getListPDF() -> [PDFData] {
        if let retrivedList: [PDFData] = retrieveArrayFromUserDefaults(forKey: Keys.pdfData) {
            return retrivedList
        }
        return []
    }
}
