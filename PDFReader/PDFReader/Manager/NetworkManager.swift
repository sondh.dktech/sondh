//
//  NetworkManager.swift
//  Magnifier Magnifying Glass 10x
//
//  Created by Pham Van Thai on 22/07/2023.
//

import Foundation
import Network

class NetworkManager {
    static let shared = NetworkManager()

    private init() {}

    private let monitor = NWPathMonitor()

    func startMonitoring() {
        monitor.start(queue: DispatchQueue.global())
    }

    func stopMonitoring() {
        monitor.cancel()
    }

    func isConnected() -> Bool {
        if monitor.currentPath.availableInterfaces.contains(where: { $0.type == .wifi }) || monitor.currentPath.availableInterfaces.contains(where: { $0.type == .cellular }) {
            return true
        }
        return false
    }
    
    func realTimeNetwork(completion: @escaping (Bool) -> Void){
        monitor.pathUpdateHandler = { value in
            print("thaipv realTimeNetwork")
            if value.availableInterfaces.contains(where: { $0.type == .wifi }) || value.availableInterfaces.contains(where: { $0.type == .cellular }) {
                completion(true)
            }else {
                completion(false)
            }
        }
    }
}
