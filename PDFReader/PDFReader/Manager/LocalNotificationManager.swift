//
//  LocalNotificationManager.swift
//  PDFReader
//
//  Created by DEV on 30/01/2024.
//

import Foundation
import UserNotifications

struct NotificationAlerts {
    var id:String
    var title:String
    var body: String
}

class LocalNotificationManager {
    var notifications = [NotificationAlerts]()
    
    func listScheduledNotifications()
    {
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in
            
            for notification in notifications {
                print(notification)
            }
        }
    }
    
    func removeNoti() {
        notifications.removeAll()
    }
    
    func schedule()
    {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            
            switch settings.authorizationStatus {
            case .notDetermined:
                break
            case .authorized, .provisional:
                self.scheduleNotifications()
            default:
                break // Do nothing
            }
        }
    }
    
    private func scheduleNotifications()
    {
        for notification in notifications
        {
            let content      = UNMutableNotificationContent()
            content.title    = notification.title
            content.body     = notification.body
            content.sound    = .default
            
            let twoDaysLater = Calendar.current.date(byAdding: .day, value: 2 , to: Date())!
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: twoDaysLater), repeats: false)
            
            let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { error in
                
                guard error == nil else { return }
                
                print("Notification scheduled! --- ID = \(notification.id)")
            }
        }
    }
}
