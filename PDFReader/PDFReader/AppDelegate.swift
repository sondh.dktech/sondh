//
//  AppDelegate.swift
//  PDFReader
//
//  Created by DEV on 19/09/2023.
//

import UIKit
import Firebase
import FirebaseRemoteConfig
import Photos
import UserNotifications
import FirebaseMessaging
import FacebookCore
import GoogleMobileAds
import FBSDKCoreKit
import Adjust
import FirebaseAnalytics

@main
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate, AdjustDelegate {

    var networkManager: NetworkManager?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if !UserDefaultsManager.shared.isFirstLoadLanguage {
            UserDefaultsManager.shared.isFirstLoadLanguage = true
            UserDefaultsManager.shared.firstSaveLanguages()
            UserDefaultsManager.shared.firstLoadData()
            let language = UserDefaultsManager.shared.setLanguageDefaults()
            Bundle.set(language: language.0)
        }
//        var data = UserDefaultsManager.shared.getListPDF()
//        let containsExamplePDF = data.contains { $0.name == "Example PDF.pdf" }
//
//        if !containsExamplePDF {
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd/MM/yyyy"
//            let currentDate = Date()
//            let formattedDate = dateFormatter.string(from: currentDate)
//            data.insert(PDFData(url: "", name: "Example PDF.pdf", date: formattedDate, size: "0.02 MB", isFavorite: false), at: 0)
//        }
//        UserDefaultsManager.shared.savePDF(listPDF: data)
        UserDefaultsManager.shared.completeShare = false
        UserDefaultsManager.shared.backgroundShare = false
        GADMobileAds.sharedInstance().start()
//        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "e3ee41c836bba02529fa14bc753715e2" ]
        networkManager = NetworkManager.shared
        networkManager?.startMonitoring()
        let yourAppToken = AdMobConstants.APP_ID_ADJUST
        let environment = ADJEnvironmentProduction as? String ?? ""
        let myConfig = ADJConfig(
            appToken: yourAppToken,
            environment: environment)
        myConfig?.logLevel = ADJLogLevelVerbose
        myConfig?.delegate = self
        //Adjust.requestTrackingAuthorization()
        Adjust.appTrackingAuthorizationStatus()
        Adjust.appDidLaunch(myConfig)
        //Must have
        FirebaseApp.configure()
        FirebaseAnalytics.Analytics.setAnalyticsCollectionEnabled(true)
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        application.registerForRemoteNotifications()
        ApplicationDelegate.shared.application(
                    application,
                    didFinishLaunchingWithOptions: launchOptions
                )
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        messaging.token { token,_ in
            guard let token = token else {
                return
            }
            print("Token\(token)")
        }
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        networkManager?.stopMonitoring()
        UserDefaultsManager.shared.removeCountShowRating()
        UserDefaultsManager.shared.removeIsShowRatingStar()
    }
    
    func adjustConversionValueUpdated(_ fineValue: NSNumber?, coarseValue: String?, lockWindow: NSNumber?) {
        print("Fine conversion value: \(fineValue ?? 0)")
        print("Coarse conversion value: \(coarseValue ?? "")")
        print("Will send before conversion value window ends: \(lockWindow?.boolValue ?? nil)")
        if let value = fineValue as? Int {
            Adjust.updateConversionValue(value)
        }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Handle the local notification when the app is active
        print("Received local notification: \(notification.request.content.title) - \(notification.request.content.body)")
        completionHandler([.alert, .sound, .badge])
    }

    // This method is called when the user taps on a notification (local or remote) that causes the app to launch.
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Handle the local notification when the app is opened from the notification
        Analytics.logEvent("tap_on_noti_\(AdMobConstants.VERSION)", parameters: nil)
        completionHandler()
    }

    private func handleLocalNotification(_ notification: UNNotification) {
        // Handle the local notification when the app is launched
        print("Received local notification: \(notification.request.content.title) - \(notification.request.content.body)")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        let rootViewController = application.windows.first(where: { $0.isKeyWindow })?.rootViewController
        if let rootViewController = rootViewController {
            if rootViewController is SplashViewController {
                return
            }
        }
    }


}

