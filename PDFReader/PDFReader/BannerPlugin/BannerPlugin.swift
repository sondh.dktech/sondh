//
//  BannerPlugin.swift
//  bannerplugin
//
//  Created by Hung Pham on 5/6/23.
//

import Foundation
import UIKit


class BannerPlugin {
    
    static let LOG_ENABLED = true
    
    class Config {
        var defaultAdUnitId: String
        var defaultBannerType: BannerType
        var isLoadNetwork: Bool
        var loadAdAfterInit = true
        
        required init(defaultAdUnitId: String, defaultBannerType: BannerType, isLoadNetwork:Bool = true) {
            self.defaultAdUnitId = defaultAdUnitId
            self.defaultBannerType = defaultBannerType
            self.isLoadNetwork = isLoadNetwork
        }
    }
    
    enum BannerType {
        case Standard,
             Adaptive,
             CollapsibleTop,
             CollapsibleBottom
    }
    
    private let rootViewController: UIViewController
    private let adContainer: UIView
    private let config: Config
    
    private var adViewController: BaseAdViewController? = nil
    
    required init(rootViewController: UIViewController, adContainer: UIView, config: Config) {
        self.rootViewController = rootViewController
        self.adContainer = adContainer
        self.config = config
        
        initViewAndConfig()
        
        if (config.loadAdAfterInit) {
            loadAd()
        }
    }
    
    private func initViewAndConfig() {
        let adUnitId = config.defaultAdUnitId
        let bannerType = config.defaultBannerType
        let isLoadNetwork = config.isLoadNetwork
        log(message: "adUnitId = \(adUnitId) " +
            " - bannerType = \(bannerType) "
        )
        
        adViewController = BaseAdViewController.Factory.getAdView(
            adUnitId: adUnitId,
            bannerType: bannerType,
            isLoadNetwork: isLoadNetwork
        )
        
        if (adViewController == nil) {
            return
        }
        
        let adViewController = adViewController!
        let adView = adViewController.view!
        
        rootViewController.addChild(adViewController)
        adView.layer.borderWidth = 0.5
        adView.layer.borderColor = UIColor.brown.cgColor
        adView.translatesAutoresizingMaskIntoConstraints = false
        adContainer.addSubview(adView)
        adContainer.addConstraints([
            NSLayoutConstraint(item: adView,
                               attribute: .top,
                               relatedBy: .equal,
                               toItem: adContainer,
                               attribute: .top, multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: adView,
                               attribute: .left,
                               relatedBy: .equal,
                               toItem: adContainer,
                               attribute: .left, multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: adView,
                               attribute: .right,
                               relatedBy: .equal,
                               toItem: adContainer,
                               attribute: .right, multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: adView,
                               attribute: .bottom,
                               relatedBy: .equal,
                               toItem: adContainer,
                               attribute: .bottom, multiplier: 1,
                               constant: 0),
        ])
        
        adViewController.didMove(toParent: rootViewController)
    }
    
    func loadAd() {
        adViewController?.loadAd()
    }
}

func log(message: String) {
    if (BannerPlugin.LOG_ENABLED) {
        print("BannerPlugin: " + message)
    }
}
