//
//  BaseAdView.swift
//  bannerplugin
//
//  Created by Hung Pham on 5/6/23.
//

import Foundation
import UIKit

class BaseAdViewController : UIViewController {
    
    required init?(coder: NSCoder) {
        // Should not be used from storyboard or xib
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        
    }
    
    func loadAd() {
        log(message: "LoadAd ...")
        loadAdInternal {
            log(message: "On load ad done ...")
        }
    }
    
    internal func loadAdInternal(onDone: @escaping () -> Void) {
        fatalError("loadAdInternal(onDone:) has not been implemented")
    }
    
    class Factory {
        static func getAdView(
            adUnitId: String,
            bannerType: BannerPlugin.BannerType,
            isLoadNetwork: Bool
        ) -> BaseAdViewController {
            switch (bannerType) {
            case BannerPlugin.BannerType.Adaptive,
                BannerPlugin.BannerType.Standard,
                BannerPlugin.BannerType.CollapsibleBottom,
                BannerPlugin.BannerType.CollapsibleTop: return BannerAdViewController(
                    adUnitId: adUnitId,
                    bannerType: bannerType,
                    isLoadNetwork: isLoadNetwork
                )
            }
        }
    }
}
