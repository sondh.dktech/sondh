//
//  BannerAdView.swift
//  bannerplugin
//
//  Created by Hung Pham on 5/6/23.
//

import Foundation
import GoogleMobileAds
import SkeletonView

class BannerAdViewController : BaseAdViewController, GADBannerViewDelegate {
    
    let adUnitId: String
    private let bannerType: BannerPlugin.BannerType
    private let bgSkeletonView = UIView()
    private let adView: GADBannerView
    private var hasSetAdSize = false
    
    private var onAdLoadDone : (() -> Void)? = nil
    
    private var isReLoad = false
    private var isCheckReload = true
    private let adRequest = GADRequest()
    private var heightBanner: NSLayoutConstraint?
    private var estimateHeightBanner: NSLayoutConstraint?
    private var isLoad = true
    private var isLoadNetwork = true
    required init?(coder: NSCoder) {
        // Should not be used from storyboard or xib
        fatalError("init(coder:) has not been implemented")
    }
    
    required init(adUnitId: String, bannerType: BannerPlugin.BannerType, isLoadNetwork: Bool) {
        self.adUnitId = adUnitId
        self.bannerType = bannerType
        self.isLoadNetwork = isLoadNetwork
        adView = GADBannerView()
        super.init()
        adView.rootViewController = self
        adView.adUnitID = adUnitId
        attachAdViewToMainView(adView: adView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isLoadNetwork {
            checkNetwork()
        }
        heightBanner = self.view.heightAnchor.constraint(equalToConstant: 0)
        estimateHeightBanner = self.view.heightAnchor.constraint(equalToConstant: 60)
        estimateHeightBanner?.isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isLoadNetwork {
            if isReLoad {
                reloadAd()
            }else {
                isReLoad = true
            }
        }
    }
    
    private func attachAdViewToMainView(adView: GADBannerView) {
        adView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(adView)
        setupSkeletonBanner()
        view.addConstraints(
            [NSLayoutConstraint(item: adView,
                                attribute: .centerY,
                                relatedBy: .equal,
                                toItem: view.safeAreaLayoutGuide,
                                attribute: .centerY,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: adView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    override func loadAdInternal(onDone: @escaping () -> Void) {
        if (!hasSetAdSize) {
            // Wait until view is rendered
            DispatchQueue.main.async {
                let adSize = self.getAdSize(bannerType: self.bannerType)
                self.adView.adSize = adSize
                
                // Update layout width & height by ad size
                if let rootView = self.view {
                    rootView.addConstraints([
                        NSLayoutConstraint(item: rootView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: adSize.size.width),
                        NSLayoutConstraint(item: rootView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: adSize.size.height)
                    ])
                }
                
                self.hasSetAdSize = true
                self.doLoadAd(onDone: onDone)
            }
        } else {
            doLoadAd(onDone: onDone)
        }
    }
    
    private func checkNetwork() {
        NetworkManager.shared.realTimeNetwork{ [weak self] value in
            guard let self = self else {return}
            if self.isLoad {
                self.isLoad = false
            }
            DispatchQueue.main.async {
                if self.view.window != nil {
                    if value {
                        self.showBanner()
                        self.reloadAd()
                    }else {
                        self.hiddenBanner()
                    }
                }
            }
            DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
                self.isLoad = true
            }
        }
    }
    
    private func reloadAd(){
        if isReLoad && isCheckReload {
            isCheckReload = false
            self.bgSkeletonView.isHidden = false
            adView.delegate = self
            adView.load(adRequest)
        }
    }
    
    private func hiddenBanner() {
        self.heightBanner?.isActive = true
        self.adView.isHidden = true
    }
    
    private func showBanner() {
        self.heightBanner?.isActive = false
        self.adView.isHidden = false
    }
    
    private func setupSkeletonBanner() {
        bgSkeletonView.backgroundColor = .white
        self.view.addSubview(bgSkeletonView)
        bgSkeletonView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bgSkeletonView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            bgSkeletonView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            bgSkeletonView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            bgSkeletonView.topAnchor.constraint(equalTo: self.view.topAnchor)
        ])
        bgSkeletonView.isSkeletonable = true
        bgSkeletonView.showAnimatedGradientSkeleton()
        
        let section1 = UIView()
        section1.backgroundColor = .white
        bgSkeletonView.addSubview(section1)
        section1.translatesAutoresizingMaskIntoConstraints = false
        let topSection1 = section1.topAnchor.constraint(equalTo: bgSkeletonView.topAnchor)
        let leadingSection1 = section1.leadingAnchor.constraint(equalTo: bgSkeletonView.leadingAnchor)
        let trailingSection1 = section1.trailingAnchor.constraint(equalTo: bgSkeletonView.trailingAnchor)
        let bottomSection1 = section1.bottomAnchor.constraint(equalTo: bgSkeletonView.bottomAnchor)
        NSLayoutConstraint.activate([topSection1, leadingSection1, trailingSection1, bottomSection1])
        
        
        let section2 = UIView()
        section2.backgroundColor = .gray
        bgSkeletonView.addSubview(section2)
        section2.translatesAutoresizingMaskIntoConstraints = false
        let topSection2 = section2.leadingAnchor.constraint(equalTo: section1.leadingAnchor, constant: 12)
        let leadingSection2 = section2.topAnchor.constraint(equalTo: section1.topAnchor, constant: 6)
        //let trailingSection2 = section2.trailingAnchor.constraint(equalTo: bgSkeletonView.trailingAnchor,constant: -12)
        let bottomSection2 = section2.heightAnchor.constraint(equalToConstant: 40)
        let widthSection2 = section2.widthAnchor.constraint(equalToConstant: 40)
        NSLayoutConstraint.activate([topSection2, leadingSection2, widthSection2, bottomSection2])
        section2.isSkeletonable = true
        section2.showAnimatedGradientSkeleton()
        
        
        let section3 = UIView()
        section3.backgroundColor = .gray
        bgSkeletonView.addSubview(section3)
        section3.translatesAutoresizingMaskIntoConstraints = false
        let topSection3 = section3.topAnchor.constraint(equalTo: section2.topAnchor, constant: 0)
        let leadingSection3 = section3.leadingAnchor.constraint(equalTo: section2.trailingAnchor,constant: 12)
        let trailingSection3 = section3.trailingAnchor.constraint(equalTo: bgSkeletonView.trailingAnchor,constant: -12)
        let bottomSection3 = section3.heightAnchor.constraint(equalToConstant: 12)
        NSLayoutConstraint.activate([topSection3, leadingSection3, trailingSection3, bottomSection3])
        section3.isSkeletonable = true
        section3.showAnimatedGradientSkeleton()
        
        let section4 = UIView()
        section4.backgroundColor = .gray
        bgSkeletonView.addSubview(section4)
        section4.translatesAutoresizingMaskIntoConstraints = false
        let topSection4 = section4.topAnchor.constraint(equalTo: section3.bottomAnchor, constant: 4)
        let leadingSection4 = section4.leadingAnchor.constraint(equalTo: section2.trailingAnchor,constant: 12)
        let trailingSection4 = section4.widthAnchor.constraint(equalToConstant: 120)
        let bottomSection4 = section4.heightAnchor.constraint(equalToConstant: 12)
        NSLayoutConstraint.activate([topSection4, leadingSection4, trailingSection4, bottomSection4])
        section4.isSkeletonable = true
        section4.showAnimatedGradientSkeleton()
        
        let section5 = UIView()
        section5.backgroundColor = .gray
        bgSkeletonView.addSubview(section5)
        section5.translatesAutoresizingMaskIntoConstraints = false
        let topSection5 = section5.topAnchor.constraint(equalTo: section4.bottomAnchor, constant: 4)
        let leadingSection5 = section5.leadingAnchor.constraint(equalTo: section2.trailingAnchor,constant: 12)
        let trailingSection5 = section5.trailingAnchor.constraint(equalTo: bgSkeletonView.trailingAnchor,constant: -62)
        let bottomSection5 = section5.heightAnchor.constraint(equalToConstant: 12)
        NSLayoutConstraint.activate([topSection5, leadingSection5, trailingSection5, bottomSection5])
        section5.isSkeletonable = true
        section5.showAnimatedGradientSkeleton()
        
    }
    
    private func getAdSize(bannerType: BannerPlugin.BannerType) -> GADAdSize {
        switch (bannerType) {
        case BannerPlugin.BannerType.Standard: return GADAdSizeBanner
        case BannerPlugin.BannerType.Adaptive,
            BannerPlugin.BannerType.CollapsibleBottom,
            BannerPlugin.BannerType.CollapsibleTop: return getAdaptiveSize()
        }
    }
    
    private func getAdaptiveSize() -> GADAdSize {
        var frame: CGRect
        
        if #available(iOS 11.0, *) {
            frame = view.frame.inset(by: view.safeAreaInsets)
        } else {
            frame = view.frame
        }
        
        var viewWidth = frame.size.width
        
        // Fallback to screen width if viewWidth = 0
        if (viewWidth == 0) {
            viewWidth = UIScreen.main.bounds.width
        }
        
        let adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(viewWidth)
        return adSize
    }
    
    private func doLoadAd(onDone: @escaping () -> Void) {
        switch (bannerType) {
        case BannerPlugin.BannerType.CollapsibleTop,
            BannerPlugin.BannerType.CollapsibleBottom:
                let position = bannerType == BannerPlugin.BannerType.CollapsibleTop ? "top" : "bottom"
                let extras = GADExtras()
                extras.additionalParameters = ["collapsible": position]
                adRequest.register(extras)
        case .Standard: break
        case .Adaptive: break
        }
        
        onAdLoadDone = onDone
        adView.delegate = self
        adView.load(adRequest)
    }
    
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        estimateHeightBanner?.isActive = false
        adView.delegate = nil
        onAdLoadDone?()
        bgSkeletonView.isHidden = true
        isCheckReload = true
        showBanner()
        bannerView.paidEventHandler = { adValue in
            PaidEventHandlerManager.shared.getPaidEventHandler(dataPaidEvent: adValue, typeAds: .bannerAds, adUnit: self.adUnitId)
        }
    }

    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("thaipv \(error.localizedDescription)")
        estimateHeightBanner?.isActive = false
        adView.delegate = nil
        onAdLoadDone?()
        bgSkeletonView.isHidden = true
        isCheckReload = true
        hiddenBanner()
    }
}
