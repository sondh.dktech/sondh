//
//  SplashViewController.swift
//  HairClipper
//
//  Created by DEV on 10/08/2023.
//

import UIKit
import Lottie
import UserNotifications
import GoogleMobileAds
import UserMessagingPlatform
import Adjust
import AppTrackingTransparency
import FirebaseAnalytics
import FirebaseRemoteConfig

class SplashViewController: UIViewController {

    @IBOutlet weak var splashImg: UIImageView!
    @IBOutlet weak var loadingAnimation: UIView!
    @IBOutlet weak var splashLbl: UILabel!
    @IBOutlet weak var splashLbl2: UILabel!
    @IBOutlet weak var splashText: UILabel!
    @IBOutlet weak var splashReader: UILabel!
    
    var interstitial: GADInterstitialAd?
    let startBtn = UIButton()
    let startLbl = UILabel()
    var ecgAnimatedView: LottieAnimationView!
    var secondsRemaining: Int = 5
    var countdownTimer: Timer?
    var isShow = false
    let bgNative = UIView()
    var plugin: NativePlugin?
    var firstLoad: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        Analytics.logEvent("splash_screen_\(AdMobConstants.VERSION)", parameters: nil)
        UserDefaultsManager.shared.isSplash = true
        UserDefaultsManager.shared.isShowRatingStar = false
        setupText()
        setupAnimation()
        UserDefaultsManager.shared.isFirstOpenChooseLanguage = false
        UserDefaultsManager.shared.isFirstOpenApp = false
        AppOpenAdManager.shared.appOpenAdManagerDelegate = self
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] success, _ in
//            self?.requestNotificationAuthorization()
//            guard success else { return }
//            print("Success in APNS registry")
//            UserDefaultsManager.shared.isShowPermissionNoti = true
//        }
        if !UserDefaultsManager.shared.isFirstOpenCMP {
            requestNotificationAuthorization()
        } else {
            setupAllAds()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        countdownTimer?.invalidate()
        countdownTimer = nil
    }
    
    func setupAllAds() {
        getKeyRevenueUS { [weak self] in
            guard let self = self else {return}
            DispatchQueue.main.async { [weak self] in
                InterSaveAdsManager.shared.loadInter()
                OnResumeManager.shared.loadAd(loadTestMode: true)
                AppOpenAdManager.shared.loadAd(loadTestMode: true)
                self?.setupNativeAds()
                self?.startTimer()
            }
        }
    }
    
    func getKeyRevenueUS(completion: @escaping () -> Void) {
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        remoteConfig.fetch { (status, error) in
            if status == .success {
                remoteConfig.activate { (changed, error) in
                    if let top10Percent = remoteConfig["top10Percent"].numberValue as? Double,
                       let top20Percent = remoteConfig["top20Percent"].numberValue as? Double,
                       let top30Percent = remoteConfig["top30Percent"].numberValue as? Double,
                       let top40Percent = remoteConfig["top40Percent"].numberValue as? Double,
                       let top50Percent = remoteConfig["top50Percent"].numberValue as? Double
                    {
                        UserDefaultsManager.shared.top10Threshold = top10Percent
                        UserDefaultsManager.shared.top20Threshold = top20Percent
                        UserDefaultsManager.shared.top30Threshold = top30Percent
                        UserDefaultsManager.shared.top40Threshold = top40Percent
                        UserDefaultsManager.shared.top50Threshold = top50Percent
                        if top10Percent != 0 && top10Percent != 0 && top10Percent != 0 && top10Percent != 0 && top10Percent != 0 {
                            UserDefaultsManager.shared.isLocationUS = true
                        }else {
                            UserDefaultsManager.shared.isLocationUS = false
                        }
                        
                    }else {
                        UserDefaultsManager.shared.isLocationUS = false
                    }
                    print("thaipv \(UserDefaultsManager.shared.top10Threshold)")
                    print("thaipv \(UserDefaultsManager.shared.top20Threshold)")
                    print("thaipv \(UserDefaultsManager.shared.top30Threshold)")
                    print("thaipv \(UserDefaultsManager.shared.top40Threshold)")
                    print("thaipv \(UserDefaultsManager.shared.top50Threshold)")
                    completion()
                }
            } else {
                print("thaipv Error fetching config: \(error)")
                UserDefaultsManager.shared.isLocationUS = false
                completion()
            }
        }
    }
    
    func requestNotificationAuthorization() {
        // Create a UMPRequestParameters object.
        let parameters = UMPRequestParameters()
        parameters.tagForUnderAgeOfConsent = false
        UMPConsentInformation.sharedInstance.requestConsentInfoUpdate(with: parameters) {
            [weak self] requestConsentError in
            guard let self = self else { return }
            
            if let consentError = requestConsentError {
                // Consent gathering failed.
                DispatchQueue.main.async { [weak self] in
                    self?.setupAllAds()
                }
                return
            }
            print("Consent gathering successed")
            // TODO: Load and present the consent form.
            UMPConsentForm.loadAndPresentIfRequired(from: self) {
                [weak self] loadAndPresentError in
                guard let self else {  return }
                if let consentError = loadAndPresentError {
                    // Consent gathering failed.
                    DispatchQueue.main.async { [weak self] in
                        self?.setupAllAds()
                    }
                    return print("loadAndPresentError: \(consentError.localizedDescription)")
                }
                
                // Consent has been gathered.
                print("Consent has been gathered")
                if UMPConsentInformation.sharedInstance.canRequestAds {
                    Analytics.logEvent("cmp_choose_\(AdMobConstants.VERSION)", parameters: nil)
                    DispatchQueue.main.async { [weak self] in
                        UserDefaultsManager.shared.isFirstOpenCMP = true
                        self?.setupAllAds()
                    }
                    print("User đồng ý")
                }
            }
        }
    }
    
    
    func setupNativeAds() {
//        if let idAd = APIManager.shared.adIDs[AdMobConstants.NATIVE_LANGUAGE] {
//        let idAd = "ca-app-pub-3940256099942544/3986624511"
        let idAd = AdMobConstants.NATIVE_LANGUAGE
        let config = NativePlugin.Config(defaultAdUnitId: idAd, defaultAdUnitIdBu: "", defaultNativeType: .NewCustom, borderBGColor: UIColor.black.cgColor, bgColorNativeAds: UIColor(rgb: 0xF5F5F5), colorButton: UIColor.red, loadTestMode: false, textColor: UIColor(rgb: 0x4F646F), buttonColor: UIColor(rgb: 0x1890FF), adColor: UIColor(rgb: 0x1890FF))
            plugin = NativePlugin(rootViewController: self, adContainer: bgNative, config: config)
//        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    func setupText() {
        splashLbl.layer.masksToBounds = true
        splashLbl.layer.cornerRadius = 6
        splashText.text = Localizable.localizedString("SplashText")
        splashLbl2.text = Localizable.localizedString("SplashText2")
        if NetworkManager.shared.isConnected() {
            splashText.isHidden = false
        } else {
            splashText.isHidden = true
        }
        if UserDefaultsManager.shared.isiPad() {
            splashImg.image = UIImage(named: "splash_ipad")
            splashLbl.widthAnchor.constraint(equalToConstant: 110).isActive = true
            splashReader.font = UIFont(name: "Inter-SemiBold", size: 56)
            splashLbl.font = UIFont(name: "Inter-Black", size: 44)
            splashLbl2.font = UIFont(name: "Inter-Bold", size: 22)
            splashText.font = UIFont(name: "Inter", size: 22)
        }
    }
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(SplashViewController.decrementCounter),
            userInfo: nil,
            repeats: true)
    }
    
    @objc func decrementCounter() {
        secondsRemaining -= 1
        if !NetworkManager.shared.isConnected(){
            countdownTimer?.invalidate()
            countdownTimer = nil
            continueStep()
        }else if secondsRemaining <= 0 {
            countdownTimer?.invalidate()
            countdownTimer = nil
            if isShow {
                AppOpenAdManager.shared.showAdIfAvailable(viewController: self)
            }else {
                continueStep()
            }
        }
    }
    
    func setupAnimation() {
        ecgAnimatedView = .init(name: "Loading")
        ecgAnimatedView.translatesAutoresizingMaskIntoConstraints = false
        loadingAnimation.addSubview(ecgAnimatedView)
        NSLayoutConstraint.activate([
            ecgAnimatedView.topAnchor.constraint(equalTo: loadingAnimation.topAnchor),
            ecgAnimatedView.bottomAnchor.constraint(equalTo: loadingAnimation.bottomAnchor),
            ecgAnimatedView.leadingAnchor.constraint(equalTo: loadingAnimation.leadingAnchor),
            ecgAnimatedView.trailingAnchor.constraint(equalTo: loadingAnimation.trailingAnchor)
        ])
        ecgAnimatedView.contentMode = .scaleAspectFill
        ecgAnimatedView.loopMode = .loop
        ecgAnimatedView.animationSpeed = 1
        ecgAnimatedView.play()
    }

    
    func continueStep() {
        NativeAdManager.shared.setNativeView(nativeView: plugin?.loadNativeAd())
        let vc = ChooseLanguageViewController()
        vc.nativeAdV = plugin?.loadNativeAd()
        navigationController?.pushViewController(vc, animated: true)
        UserDefaultsManager.shared.isSplash = false
    }
}


extension SplashViewController: AppOpenAdManagerDelegate {
    func appWillOpenAds(_ appOpenAdManager: AppOpenAdManager, isSuccess: Bool) {
        if isSuccess {
            isShow = true
        }
        secondsRemaining = 0
    }

    func appOpenAdManagerAdDidComplete(_ appOpenAdManager: AppOpenAdManager) {
        if secondsRemaining <= 0 {
            continueStep()
        }
    }
}
