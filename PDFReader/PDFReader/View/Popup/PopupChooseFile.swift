//
//  PopupChooseFile.swift
//  PDFReader
//
//  Created by DEV on 07/12/2023.
//

import Foundation
import UIKit
class PopupChooseFile: UIView {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var photoBtn: UIButton!
    @IBOutlet weak var photoLbl: UILabel!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var cameraLbl: UILabel!
    @IBOutlet weak var fileView: UIView!
    @IBOutlet weak var fileBtn: UIButton!
    @IBOutlet weak var fileLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var cancelLbl: UILabel!
    @IBOutlet weak var line1View: UIView!
    @IBOutlet weak var line2View: UIView!
    @IBOutlet weak var line3View: UIView!
    @IBOutlet weak var line4View: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
        self.isOpaque = false
    }
    
    func xibSetup(frame: CGRect) {
        let view = loadXib()
        view.frame = frame
        addSubview(view)
    }
    
    func loadXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PopupChooseFile", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view!
    }
}
