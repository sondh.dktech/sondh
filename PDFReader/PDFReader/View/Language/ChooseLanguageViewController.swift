//
//  ChooseLanguageViewController.swift
//  HairClipper
//
//  Created by DEV on 10/08/2023.
//

import UIKit
import GoogleMobileAds
import SkeletonView
import FirebaseAnalytics
import Lottie

class ChooseLanguageViewController: UIViewController {

    @IBOutlet weak var loadingAnimation: UIView!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var backBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var nativeAd: UIView!
    @IBOutlet weak var heightNativeAds: NSLayoutConstraint!
    @IBOutlet weak var backImg: UIImageView!

    var ecgAnimatedView: LottieAnimationView!
    var nativeAdView: UIView?
    var heightConstraint: NSLayoutConstraint?
    var skeletonView = UIView()
    var isBackupAdLoading = false
    var selectLanguage: Int = -1
    var selectedLanguage: Int = 0
    var isFirstLoad: Bool = true
    var nativeAdV: GADNativeAdView?
    var plugin: NativePlugin?
    private var languages: [Languages] = []
    var isLoadNative: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        setupView()
        setupTableView()
        if UserDefaultsManager.shared.isiPad() {
            languageBtn.widthAnchor.constraint(equalToConstant: 110).isActive = true
            languageBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
            languageBtn.layer.cornerRadius = 15
            languageBtn.layer.masksToBounds = true
        } else {
            languageBtn.layer.cornerRadius = 10
            languageBtn.layer.masksToBounds = true
        }
        
        if !UserDefaultsManager.shared.isFirstOpenChooseLanguage {
            backBtn.isHidden = true
            Analytics.logEvent("language_screen_\(AdMobConstants.VERSION)", parameters: nil)
            backImg.isHidden = true
            if UserDefaultsManager.shared.isiPad() {
                languageLbl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
            } else {
                languageLbl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive = true
            }
        } else {
            backImg.isHidden = false
            backBtn.isHidden = false
            backBtnWidth.constant = view.frame.width * 0.061
        }
        NotificationCenter.default.addObserver(self, selector: #selector(showApply), name: NSNotification.Name("loadNative"), object: nil)
    }
    
    @objc func showApply() {
        loadingAnimation.isHidden = true
        languageBtn.isHidden = false
    }
    
    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("loadNative"), object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        setupAnimation()
        if UserDefaultsManager.shared.isFirstOpenChooseLanguage {
            loadingAnimation.isHidden = false
            languageBtn.isHidden = true
            setupNativeAds()
        } else {
            if let nativeAdView = NativeAdManager.shared.getNativeView() {
                nativeAd.addSubview(nativeAdView)
                nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                NSLayoutConstraint.activate([
                    nativeAdView.topAnchor.constraint(equalTo: nativeAd.topAnchor),
                    nativeAdView.bottomAnchor.constraint(equalTo: nativeAd.bottomAnchor),
                    nativeAdView.leadingAnchor.constraint(equalTo: nativeAd.leadingAnchor),
                    nativeAdView.trailingAnchor.constraint(equalTo: nativeAd.trailingAnchor)
                ])
                loadingAnimation.isHidden = true
                languageBtn.isHidden = false
            } else {
                loadingAnimation.isHidden = false
                languageBtn.isHidden = true
                setupNativeAds()
            }
        }
        languageBtn.setTitle(Localizable.localizedString("LanguageButton"), for: .normal)
        languages = UserDefaultsManager.shared.getLanguage()
        for (index, item) in languages.enumerated() {
            if item.code {
                selectLanguage = index
                selectedLanguage = index
            }
        }
        if !NetworkManager.shared.isConnected() {
            nativeAd.heightAnchor.constraint(equalToConstant: 0).isActive = true
            nativeAd.isHidden = true
        }
    }
    
    func setupAnimation() {
        ecgAnimatedView = .init(name: "loading_language")
        ecgAnimatedView.translatesAutoresizingMaskIntoConstraints = false
        loadingAnimation.addSubview(ecgAnimatedView)
        NSLayoutConstraint.activate([
            ecgAnimatedView.topAnchor.constraint(equalTo: loadingAnimation.topAnchor),
            ecgAnimatedView.bottomAnchor.constraint(equalTo: loadingAnimation.bottomAnchor),
            ecgAnimatedView.leadingAnchor.constraint(equalTo: loadingAnimation.leadingAnchor),
            ecgAnimatedView.trailingAnchor.constraint(equalTo: loadingAnimation.trailingAnchor)
        ])
        ecgAnimatedView.contentMode = .scaleAspectFill
        ecgAnimatedView.loopMode = .loop
        ecgAnimatedView.animationSpeed = 1
        ecgAnimatedView.play()
    }
    
    func setupNativeAds() {
//        let idAd = "ca-app-pub-3940256099942544/3986624511"
        let idAd = AdMobConstants.NATIVE_LANGUAGE
        let config = NativePlugin.Config(defaultAdUnitId: idAd, defaultAdUnitIdBu: "", defaultNativeType: .NewCustom, borderBGColor: UIColor.black.cgColor, bgColorNativeAds: UIColor(rgb: 0xF5F5F5), colorButton: UIColor(rgb: 0x1890FF), loadTestMode: false, textColor: UIColor(rgb: 0x4F646F), buttonColor: UIColor(rgb: 0x1890FF), adColor: UIColor(rgb: 0x1890FF))
            let _ = NativePlugin(rootViewController: self, adContainer: nativeAd, config: config)
        
    }
    
    func setupView() {
        languageLbl.text = Localizable.localizedString("LanguageTitle")
        if UserDefaultsManager.shared.isiPad() {
            languageLbl.font = UIFont(name: "Inter-Bold", size: 28)
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive = true
            languageBtn.titleLabel?.font = UIFont(name: "Inter-SemiBold", size: 18)
        }
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib(nibName: "ChooseLanguageTableViewCell", bundle: nil), forCellReuseIdentifier: "ChooseLanguageTableViewCell")
    }
    @IBAction func chooseLanguage(_ sender: Any) {
        UserDefaultsManager.shared.isFirstOpenChooseLanguage = true
        if selectLanguage != selectedLanguage {
            languages[selectLanguage].code = true
            languages[selectedLanguage].code = false
            UserDefaultsManager.shared.saveLanguage(listLanguages: languages)
            Bundle.set(language: languages[selectLanguage].language)
        }
        if !UserDefaultsManager.shared.isFirstOpenApp {
            let introVc = IntroViewController()
            navigationController?.pushViewController(introVc, animated: true)
        } else {
            navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func back(_ sender: Any) {
//        NotificationCenter.default.post(name: NSNotification.Name("reloadBanner"), object: nil)
        navigationController?.popViewController(animated: true)
    }
//
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("reloadLanguage"), object: nil)
//    }
    
}

extension ChooseLanguageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectLanguage = indexPath.row
        tableView.reloadData()
    }
}

extension ChooseLanguageViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UserDefaultsManager.shared.isiPad() {
            return 100
        } else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseLanguageTableViewCell") as! ChooseLanguageTableViewCell
        if selectLanguage == indexPath.row {
            if isFirstLoad {
                isFirstLoad = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                    cell.setupCellSelected()
                }
            } else {
                cell.setupCellSelected()
            }
           
        } else {
            cell.setupView()
        }
        cell.nationLbl.text = languages[indexPath.row].language.name
        cell.nationImg.image = UIImage(named: languages[indexPath.row].language.image)
        return cell
    }
}

//extension ChooseLanguageViewController: GADAdLoaderDelegate, GADNativeAdLoaderDelegate, GADVideoControllerDelegate {
//    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
//        if !isBackupAdLoading {
//            isBackupAdLoading = true
//            guard let idNative = APIManager.shared.adIDs[AdMobConstants.NATIVE_LANGUAGE_2] else { return }
//            self.adLoader = GADAdLoader(adUnitID: idNative, rootViewController: self, adTypes: [.native], options: nil)
//            self.adLoader.delegate = self
//            self.adLoader.load(GADRequest())
//        } else {
//            heightNativeAds.constant = 0
//            nativeAd.isHidden = true
//        }
//    }
//
//    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
//        nativeManager.showDataNativeAds(nativeAd: nativeAd, nativeView: nativeAdView, vc: self, skeletonView: skeletonView, textColor: .white)
//    }
//
//    func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {
//        print("Video playback has ended.")
//    }
//
//}

