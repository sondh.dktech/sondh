//
//  ChooseLanguageTableViewCell.swift
//  Wallpaper
//
//  Created by DEV on 25/07/2023.
//

import UIKit

class ChooseLanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var nationImg: UIImageView!
    @IBOutlet weak var nationLbl: UILabel!
    @IBOutlet weak var chooseImg: UIImageView!
    
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        bgView.layer.cornerRadius = 10
        bgView.layer.masksToBounds = true
        nationLbl.textColor = UIColor(rgb: 0x000000)
        bgView.backgroundColor = UIColor(rgb: 0xF5F5F5)
        if UserDefaultsManager.shared.isiPad() {
            nationLbl.font = UIFont(name: "Inter", size: 30)
            nationImg.widthAnchor.constraint(equalToConstant: 35).isActive = true
            nationImg.heightAnchor.constraint(equalToConstant: 35).isActive = true
            chooseImg.widthAnchor.constraint(equalToConstant: 30).isActive = true
            chooseImg.heightAnchor.constraint(equalToConstant: 30).isActive = true
        }
        setupView()
    }
    
    func setupView() {
        chooseImg.image = UIImage(named: "language_off")
    }
    
    func setupCellSelected() {
        chooseImg.image = UIImage(named: "language_on")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
