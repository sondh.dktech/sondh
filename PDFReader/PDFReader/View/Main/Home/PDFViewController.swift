//
//  PDFViewController.swift
//  PDFReader
//
//  Created by DEV on 21/09/2023.
//

import UIKit
import PDFKit
import MobileCoreServices

class PDFViewController: UIViewController, PDFViewDelegate {

    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var funcStackView: UIStackView!
    @IBOutlet weak var fileNameLbl: UILabel!
    @IBOutlet weak var pageNumberLbl: UILabel!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var removeBtn: UIButton!
    
    var currentSearchIndex = 0
    var pdfView = PDFView()
    var pdfURL: String?
    var isHorizontal: Bool = false
    var isLightMode: Bool = true
    var pdfName: String = ""
    let jumpPopup = PopupCollectionView()
    let backgroundView = UIView()
//    var getDocument: (Bool) -> Void
    var pdfDocument: PDFDocument?
    var pageCount: Int?
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(pdfView)
        navigationController?.setNavigationBarHidden(true, animated: true)
        if UserDefaultsManager.shared.countShowRating == 2 {
            showRating()
        }
        removeBtn.layer.cornerRadius = 5
        removeBtn.layer.masksToBounds = true
        txtField.borderStyle = .none
        txtField.returnKeyType = .search
        txtField.layer.cornerRadius = 5
        fileNameLbl.text = pdfName
        txtField.delegate = self
        pdfView.backgroundColor = UIColor(rgb: 0xD9D9D9)
        NotificationCenter.default.addObserver(self, selector: #selector(pageDidChange(notification:)), name: NSNotification.Name.PDFViewPageChanged, object: nil)
        pdfView.document = pdfDocument
        if let pageCount = pdfView.document?.pageCount {
            self.pageNumberLbl.text = "1 o \(pageCount)"
        } else {
            // Handle the case where pageCount is nil
            self.pageNumberLbl.text = "1 o N/A"
            // Or you can choose to return or log an error, depending on your use case
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.dismiss(animated: true, completion: nil)
            self.pdfView.maxScaleFactor = 3;
            self.pdfView.minScaleFactor = self.pdfView.scaleFactorForSizeToFit;
            self.pdfView.autoScales = true;
            self.pdfView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
        txtField.tag = 2
        setupScrolling()
        setupBanner()
        NotificationCenter.default.addObserver(self, selector: #selector(dismissKeyboard), name: NSNotification.Name("DismissKeyboard"), object: nil)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showRating() {
        if !UserDefaultsManager.shared.isShowRatingStar && !UserDefaultsManager.shared.isRatingStar {
            UserDefaultsManager.shared.isShowRatingStar = true
            StarRating.shared.addRating(vc: self, title: Localizable.localizedString("RateTitle"), contentLow: Localizable.localizedString("RateDesc1"), contentMedium: Localizable.localizedString("RateDesc2"), contentHigh: Localizable.localizedString("RateDesc3"), remind: Localizable.localizedString("RateRemind"), submit: Localizable.localizedString("RateSubmit"), icon: UIImageView(image: UIImage(named: "AppIcon")), nameApp: "PDF Reader & PDF Viewer App", cancelText: Localizable.localizedString("RateCancel"), sendText: Localizable.localizedString("RateSend"), placeHolderTextView: Localizable.localizedString("RatePlaceholder"), emailAddress: "vapp.expert.help@gmail.com", yourAppId: "")
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupSearch()
    }
    
    func setupSearch() {
        if isSearching {
            fileNameLbl.isHidden = true
            pageNumberLbl.isHidden = true
            txtField.isHidden = false
            removeBtn.isHidden = false
            txtField.becomeFirstResponder()
        } else {
            removeBtn.isHidden = true
            fileNameLbl.isHidden = false
            pageNumberLbl.isHidden = false
            txtField.isHidden = true
        }
    }
    
    func getPDFFileURL(fileName: String) -> URL? {
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                return documentsDirectory.appendingPathComponent(fileName)
            }
            return nil
        }
    
    override func viewDidLayoutSubviews() {
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pdfView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor),
            pdfView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pdfView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pdfView.bottomAnchor.constraint(equalTo: funcStackView.topAnchor)
        ])
    }
    
    func setupBanner() {
//        if let id = APIManager.shared.adIDs[AdMobConstants.BANNER_RESULT_COLLAPSE] {
//        let id = "ca-app-pub-3940256099942544/2934735716"
        let id = AdMobConstants.BANNER_COLLAPSE_DETAIL
        let config = BannerPlugin.Config(defaultAdUnitId: id, defaultBannerType: .CollapsibleBottom)
                    let _ = BannerPlugin(rootViewController: self, adContainer: bannerView, config: config)
//        }
    }
    
    @objc func pageDidChange(notification: Notification) {
        updatePageNumberLabel()
    }
    
    func updatePageNumberLabel() {
        let currentPage = pdfView.currentPage?.pageRef?.pageNumber ?? 0
        if let pageCount = pdfView.document?.pageCount {
            self.pageNumberLbl.text = "\(currentPage) o \(pageCount)"
        } else {
            // Handle the case where pageCount is nil
            self.pageNumberLbl.text = "1 o N/A"
            // Or you can choose to return or log an error, depending on your use case
        }
    }
    
    func setupPop() {
        backgroundView.frame = view.bounds
        backgroundView.backgroundColor = .systemGray
        backgroundView.layer.opacity = 0.9
        view.addSubview(backgroundView)
        jumpPopup.layer.cornerRadius = 12
        jumpPopup.layer.masksToBounds = true
        jumpPopup.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(jumpPopup)
        NSLayoutConstraint.activate([
            jumpPopup.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1),
            jumpPopup.centerYAnchor.constraint(equalToSystemSpacingBelow: view.centerYAnchor, multiplier: 0.7),
            jumpPopup.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.89),
            jumpPopup.heightAnchor.constraint(equalToConstant: 210)
        ])
        jumpPopup.cancelLbl.text = Localizable.localizedString("CancelBtn")
        jumpPopup.confirmLbl.text = Localizable.localizedString("ConfirmJump")
        jumpPopup.jumpLbl.text = Localizable.localizedString("PageJump")
        jumpPopup.cancelBtn.layer.cornerRadius = 8
        jumpPopup.confirmBtn.layer.cornerRadius = 8
        jumpPopup.txtField.attributedPlaceholder = NSAttributedString(
                string: Localizable.localizedString("JumpPlaceholder"),
                attributes: [
                    .foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.lightGray
                ]
            )
        jumpPopup.cancelBtn.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        jumpPopup.confirmBtn.addTarget(self, action: #selector(jumpToPage), for: .touchUpInside)
        jumpPopup.txtField.keyboardType = .numberPad
        jumpPopup.txtField.tag = 1
        jumpPopup.txtField.delegate = self
        jumpPopup.invalidText.text = Localizable.localizedString("Invalid")
        jumpPopup.invalidText.isHidden = true
    }
    
    @objc func cancel() {
        jumpPopup.txtField.text = ""
        backgroundView.removeFromSuperview()
        jumpPopup.removeFromSuperview()
    }
    
    @objc func jumpToPage() {
        guard let pageCount = pdfView.document?.pageCount else { return }
        if let pageNumber = jumpPopup.txtField.text {
            if pageNumber != "" {
                if pageNumber.isNumeric {
                    if Int(pageNumber) ?? 1 > pageCount || Int(pageNumber) == 0 {
                        jumpPopup.invalidText.isHidden = false
                    } else {
                        if let page = pdfView.document?.page(at: (Int(pageNumber) ?? 1) - 1) {
                            pdfView.go(to: page)
                            backgroundView.removeFromSuperview()
                            jumpPopup.removeFromSuperview()
                            jumpPopup.txtField.text = ""
                        }
                    }
                } else {
                    jumpPopup.invalidText.isHidden = false
                }
            } else {
                jumpPopup.invalidText.isHidden = false
            }

        }

    }

    @IBAction func jump(_ sender: Any) {
        setupPop()
    }
    
    @IBAction func rotate(_ sender: Any) {
        isHorizontal.toggle()
        setupScrolling()
    }
    
    @IBAction func back(_ sender: Any) {
        if isSearching {
            isSearching = false
            setupSearch()
            txtField.text = ""
            view.endEditing(true)
            removeAllAnnotations(from: pdfView.document!)
        } else {
            navigationController?.popToRootViewController(animated: false)
        }
    }
    
    @IBAction func removeText(_ sender: Any) {
        txtField.text = ""
        removeAllAnnotations(from: pdfView.document!)
    }
    
    func addHighlightAnnotation(to page: PDFPage, at textRange: PDFSelection) {
        let highlight = PDFAnnotation(bounds: textRange.bounds(for: page), forType: .highlight, withProperties: nil)
        highlight.color = UIColor.yellow // Set the highlight color as desired
        page.addAnnotation(highlight)
    }
    
    func searchAndNavigateToText(_ searchText: String) {
//        if let document = pdfView.currentPage?.document {
//            DispatchQueue.global().async { [weak self] in
//                let selections = document.findString(searchText, withOptions: .caseInsensitive)
//                DispatchQueue.main.async {
//                    for selection in selections {
//                        self?.addHighlightAnnotation(to: selection.pages.first!, at: selection)
//                    }
//                }
//            }
//        }
        if let testCu2 = pdfView.currentPage?.string?.data(using: .utf8) {
            guard let testCu = String(data: testCu2, encoding: .utf8) else {
                    return
                }
            
            var xxx  = Array(testCu)
            var array: [Int] = Array(repeating: 0, count: xxx.count)
            for i in 0 ..< xxx.count {
//                if let unicodeScalar = xxx[i].unicodeScalars.first {
//                    if unicodeScalar.isASCII {
//                        print("Ký tự là mã ASCII")
//                        xxx[i] = " "
//                    } else {
//                        print("Ký tự là mã Unicode")
//                        xxx[i] = " "
//                    }
//                }
                if isCharacterDiacritic(character: xxx[i]) {
                    if i == 0 {
                        array[i] = 1
                    } else {
                        array[i] = array[i-1]+1
                    }
                } else {
                    if i == 0 {
                        array[i] = 0
                    } else {
                        array[i] = array[i-1]
                    }
                
                }
            }
            print("thaipv \(xxx)")
            print("trannghia \(testCu)")
            var searchRange = testCu.startIndex..<testCu.endIndex
            var foundRanges: [Range<String.Index>] = []

            while let range = testCu.range(of: searchText, options: [.caseInsensitive], range: searchRange) {
                // Chuỗi được tìm thấy, lưu vị trí vào mảng
                foundRanges.append(range)
                
                // Cập nhật searchRange để bắt đầu tìm kiếm ở vị trí tiếp theo
                searchRange = range.upperBound..<testCu.endIndex
            }
            
            // In ra các vị trí được tìm thấy
            for range in foundRanges {
                let startIndex = testCu.distance(from: testCu.startIndex, to: range.lowerBound)
                        let endIndex = testCu.distance(from: testCu.startIndex, to: range.upperBound)
                if let startPage = pdfView.currentPage, let endPage = pdfView.currentPage {
                    let startCharIndex = startIndex + array[startIndex] // Chỉ số ký tự bắt đầu trên trang đầu tiên.
                    let endCharIndex = endIndex + array[endIndex] - 1 // Chỉ số ký tự kết thúc trên trang thứ hai.

                    if let selection = pdfView.document?.selection(from: startPage, atCharacterIndex: startCharIndex, to: endPage, atCharacterIndex: endCharIndex) {
                        // Sử dụng selection ở đây.
                        //print("thaipv Đã tạo được PDFSelection.")
                        addHighlightAnnotation(to: startPage, at: selection)
                        
                    } else {
                        print("thaipv Không thể tạo PDFSelection.")
                    }
                } else {
                    print("thaipv Không tìm thấy trang PDF.")
                }
                        print("thaipv Vị trí bắt đầu: \(startIndex)")
                        print("thaipv Vị trí kết thúc: \(endIndex)")
                        print("thaipv Độ dài của chuỗi: \(endIndex - startIndex)")
            }
        }

    }
    
    func removeAllAnnotations(from pdfDocument: PDFDocument) {
        for pageIndex in 0 ..< pdfDocument.pageCount {
            if let page = pdfDocument.page(at: pageIndex) {
                page.annotations.forEach { annotation in
                    page.removeAnnotation(annotation)
                }
            }
        }
    }
    
    func isCharacterDiacritic(character: Character) -> Bool {
        // Chuyển đổi ký tự thành chuỗi
        let str = String(character)
        let scalars = str.unicodeScalars
        for scalar in scalars {
            if scalar.properties.isDiacritic {
                return true // Ký tự có dấu
            }
        }
        return false // Ký tự không có dấu
    }
    
//    func searchForText(_ searchText: String) {
//        guard let pdfDocument = pdfView.document else {
//            return
//        }
//
//        let searchResults = pdfDocument.findString(searchText, withOptions: .caseInsensitive)
//
//        if !searchResults.isEmpty {
//            // Display the first search result
//            let firstResult = searchResults[0]
//            pdfView.setCurrentSelection(firstResult, animate: true)
//        } else {
//            // Handle no search results found
//        }
//    }
    
//    @IBAction func nextText(_ sender: Any) {
//        guard let pdfDocument = pdfView.document else {
//                return
//            }
//
//        if currentSearchIndex < pdfDocument.findString(txtField.text ?? "").count - 1 {
//                currentSearchIndex += 1
//            let nextResult = pdfDocument.findString(txtField.text ?? "")[currentSearchIndex]
//                pdfView.setCurrentSelection(nextResult, animate: true)
//            } else {
//                // Handle reaching the end of search results
//            }
//    }
    
    
    func setupScrolling() {
        let currentPage = pdfView.currentPage?.pageRef?.pageNumber ?? 0
        if isHorizontal {
            pdfView.displayDirection = .horizontal
            if let page = pdfView.document?.page(at: currentPage - 1) {
                pdfView.go(to: page)
            }
        } else {
            pdfView.displayDirection = .vertical
            if let page = pdfView.document?.page(at: currentPage - 1) {
                pdfView.go(to: page)
            }
        }
    }
    
    @IBAction func share(_ sender: Any) {
//        if pdfURL == "Example PDF.pdf" {
//            guard let pdfURL = Bundle.main.url(forResource: "Example PDF", withExtension: "pdf") else { return }
//            let activityVC = UIActivityViewController(activityItems: [pdfURL], applicationActivities: nil)
//            if UserDefaultsManager.shared.isiPad() {
//                if activityVC.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
//                    activityVC.popoverPresentationController?.sourceView = sender as? UIView
//                                   }
//            }
//            present(activityVC, animated: true, completion: nil)
//        } else {
            guard let pdfFilePath = self.pdfURL, let url = URL(string: pdfFilePath) else { return }
            let activityVC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            if UserDefaultsManager.shared.isiPad() {
                if activityVC.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    activityVC.popoverPresentationController?.sourceView = sender as? UIView
                                   }
            }
            present(activityVC, animated: true, completion: nil)
//        }
    }
    
    @IBAction func lightModeToggle(_ sender: Any) {
        isSearching = true
        setupSearch()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.PDFViewPageChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("DismissKeyboard"), object: nil)
    }
    
}

extension PDFViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchAndNavigateToText(textField.text ?? "")

        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                if string.isEmpty && range.length == 1 {
                    removeAllAnnotations(from: pdfView.document!)
                }
        if textField.tag == 1 {
            let maxLength = 15
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            
            return newString.count <= maxLength
        }
        
        if textField.tag == 2 {
//            removeAllAnnotations(from: pdfView.document!)
//            if let oldString = textField.text {
//                let newString = oldString.replacingCharacters(in: Range(range, in: oldString)!, with: string)
//                searchAndNavigateToText(newString)
//            }
        }
        return true
    }
}

