//
//  HomeViewController.swift
//  PDFReader
//
//  Created by DEV on 19/09/2023.
//

import UIKit
import PDFKit
import Lottie
import Photos
import AVFoundation
import PhotosUI

class HomeViewController: UIViewController, UIDocumentPickerDelegate, PHPickerViewControllerDelegate {
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        print("abc")
    }
    
    
    @IBOutlet weak var importImg: UIImageView!
    @IBOutlet weak var noticeLbl1: UILabel!
    @IBOutlet weak var noticeLbl2: UILabel!
    @IBOutlet weak var importBtn: UIButton!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchImg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var titleLbl1: UILabel!
    @IBOutlet weak var titleLbl2: UILabel!
    @IBOutlet weak var importLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var numberItems: UILabel!
    @IBOutlet weak var addFilesBtn: UIButton!
    
    var data: [PDFData] = []
    var ecgAnimatedView = LottieAnimationView()
    let customPopup = PopupCollectionView()
    let popupFailed = PopupFailedView()
    let backgroundView = UIView()
    var currentFile: Int?
    var isSearching: Bool = false
    var filterData: [PDFData] = []
    var isDuplicate: Bool = false
    var tapGesture: UITapGestureRecognizer?
    var isShowKeyboard: Bool = false
    var removeTapGesture: UITapGestureRecognizer?
    var popupAlert = PopupChooseFile()
    var picker: PHPickerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("tap_on_noti\(UserDefaultsManager.shared.isOpenNoti)")
        self.overrideUserInterfaceStyle = .light
        view.backgroundColor = UIColor(rgb: 0xF5F5F5)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture!)
        tapGesture?.isEnabled = false
        tapGesture?.cancelsTouchesInView = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissKeyboard), name: NSNotification.Name("DismissKeyboard"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        setupBackgroundView()
        setupTableView()
        setupData()
        setupText()
        setupView()
        tableView.reloadData()
        removeBtn.layer.cornerRadius = 5
        removeBtn.layer.masksToBounds = true
        txtField.borderStyle = .none
        txtField.layer.cornerRadius = 5
        if filterData.count < 2 {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Item"))"
        } else {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Items"))"
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isSearching = false
        txtField.text = ""
        setupView()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        isShowKeyboard = true
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        isShowKeyboard = false
    }
    
    @objc func dismissKeyboard() {
        if isShowKeyboard {
            view.endEditing(true)
            tapGesture?.isEnabled = false
            tapGesture?.cancelsTouchesInView = false
        }
    }
    
    
    func setupText() {
        importLbl.text = Localizable.localizedString("Import")
        if UserDefaultsManager.shared.isiPad() {
            addFilesBtn.widthAnchor.constraint(equalToConstant: 90).isActive = true
            addFilesBtn.heightAnchor.constraint(equalToConstant: 90).isActive = true
            addFilesBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
            titleLbl1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
            titleLbl1.widthAnchor.constraint(equalToConstant: 102).isActive = true
            titleLbl1.font = UIFont(name: "Inter-Bold", size: 40)
            titleLbl2.font = UIFont(name: "Inter-Bold", size: 40)
            noticeLbl1.font = UIFont(name: "Inter", size: 20)
            noticeLbl2.font = UIFont(name: "Inter", size: 20)
            importLbl.font = UIFont(name: "Inter-Bold", size: 20)
            titleView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            searchBtn.heightAnchor.constraint(equalToConstant: 32).isActive = true
            searchBtn.widthAnchor.constraint(equalToConstant: 32).isActive = true
            searchBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -60).isActive = true
        }
    }
    
    @IBAction func removeText(_ sender: Any) {
        txtField.text = ""
        filterData = data
        if filterData.count < 2 {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Item"))"
        } else {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Items"))"
        }
        tableView.reloadData()
    }
    
    func setupAlert() {
        if let parent = self.parent {
            backgroundView.frame = parent.view.bounds
            backgroundView.backgroundColor = .systemGray
            backgroundView.layer.opacity = 0.8
            parent.view.addSubview(backgroundView)
            parent.view.addSubview(popupAlert)
            popupAlert.layer.cornerRadius = 12
            popupAlert.layer.masksToBounds = true
            popupAlert.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                popupAlert.centerXAnchor.constraint(equalTo: parent.view.centerXAnchor),
                popupAlert.centerYAnchor.constraint(equalTo: parent.view.centerYAnchor),
                popupAlert.heightAnchor.constraint(equalToConstant: 280),
                popupAlert.widthAnchor.constraint(equalToConstant: 300),
            ])
            popupAlert.photoLbl.text = Localizable.localizedString("PhotosOption")
            popupAlert.titleLbl.text = Localizable.localizedString("SourceTitle")
            popupAlert.cameraLbl.text = Localizable.localizedString("CameraOption")
            popupAlert.fileLbl.text = Localizable.localizedString("FileOption")
            popupAlert.cancelLbl.text = Localizable.localizedString("CancelAccessCamera")
            popupAlert.photoBtn.addTarget(self, action: #selector(requestPhotoLibraryPermission), for: .touchUpInside)
            popupAlert.cameraBtn.addTarget(self, action: #selector(checkAccessCamera), for: .touchUpInside)
            popupAlert.fileBtn.addTarget(self, action: #selector(openFilePicker), for: .touchUpInside)
            popupAlert.cancelBtn.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
        }
    }
    
    @objc func closePopup() {
        popupAlert.removeFromSuperview()
        backgroundView.removeFromSuperview()
    }
    
    
    func setupView() {
        if isSearching {
            addFilesBtn.isHidden = true
            importImg.isHidden = true
            backBtn.isHidden = false
            backImg.isHidden = false
            txtField.isHidden = false
            txtField.delegate = self
            titleLbl1.isHidden = true
            titleLbl2.isHidden = true
            removeBtn.isHidden = false
            searchBtn.isHidden = true
            searchImg.isHidden = true
        } else {
            addFilesBtn.isHidden = false
            importImg.isHidden = false
            backBtn.isHidden = true
            backImg.isHidden = true
            txtField.isHidden = true
            titleLbl1.isHidden = false
            titleLbl2.isHidden = false
            removeBtn.isHidden = true
            searchBtn.isHidden = false
            searchImg.isHidden = false
        }
        if data.isEmpty {
            numberItems.isHidden = true
            tableView.isHidden = true
            searchBtn.isHidden = true
            searchImg.isHidden = true
        } else {
            numberItems.isHidden = false
            searchBtn.isHidden = false
            tableView.isHidden = false
            searchImg.isHidden = false
        }
    }
    
    func setupData() {
        data = UserDefaultsManager.shared.getListPDF()
        filterData = data
    }
    
    func setupTableView() {
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
    }
    
    func isPDFFile(fileURL: URL) -> Bool {
        // Get the file's path extension (e.g., "pdf" from "example.pdf")
        do {
            let pdfData = try Data(contentsOf: fileURL)
            // Use the pdfData as needed
            if pdfData.isPDF {
                return true
            } else {
                return false
            }
        } catch {
            print("Error loading PDF data: \(error.localizedDescription)")
            return false
        }
    }
    
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "\(fileName)"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                if self.isPDFFile(fileURL: url!) {
                    try pdfData?.write(to: actualPath, options: .atomic)
                }
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
    
    func pdfFileAlreadySaved(url:String, fileName:String)-> Bool {
        var status = false
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for content in contents {
                    if URL(string: content.description)!.lastPathComponent == fileName {
                        status = true
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
        return status
    }
    
    func showSavedPdf(url:String, fileName:String) {
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for content in contents {
                    if URL(string: content.description)!.lastPathComponent == fileName {
                        let vc = PDFViewController()
                        vc.pdfDocument = PDFDocument(url: URL(string: content.absoluteString)!)
                        vc.pdfURL = content.absoluteString
                        vc.pdfName = fileName
                        UserDefaultsManager.shared.currentPDF = fileName
                        navigationController?.pushViewController(vc, animated: true)
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }
    
    func setupBackgroundView() {
        noticeLbl1.text = Localizable.localizedString("NoHomeFile1_Des")
        noticeLbl2.text = Localizable.localizedString("NoHomeFile2_Des")
        importBtn.layer.cornerRadius = 8
        importBtn.layer.masksToBounds = true
        
    }
    
    @IBAction func importFile(_ sender: Any) {
//        setupAlert()
        let vc = ImportViewController()
        vc.sheetPresentationController?.detents = [.medium()]
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @IBAction func importFile2(_ sender: Any) {
//        setupAlert()
        let vc = ImportViewController()
        vc.sheetPresentationController?.detents = [.medium()]
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @objc func requestPhotoLibraryPermission() {
        UserDefaultsManager.shared.isShowInter = true
        let accessLevel: PHAccessLevel = .readWrite
        DispatchQueue.main.async { [weak self] in
            PHPhotoLibrary.requestAuthorization(for: accessLevel) { [weak self] status in
                switch status {
                case .limited:
                    DispatchQueue.main.async { [weak self] in
                        UserDefaultsManager.shared.isShowInter = false
                        let vc = LibraryViewController()
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }

                case .authorized:
                    UserDefaultsManager.shared.isFullAccess = true
                    DispatchQueue.main.async { [weak self] in
                        UserDefaultsManager.shared.isShowInter = false
                        let vc = LibraryViewController()
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                case .denied, .restricted:
                    DispatchQueue.main.async { [weak self] in
                        UserDefaultsManager.shared.isShowInter = false
                        let vc = PermissionViewController()
                        vc.isCamera = false
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                case .notDetermined:
                    print("abc")
                    UserDefaultsManager.shared.isShowInter = false
                @unknown default:
                    UserDefaultsManager.shared.isShowInter = false
                    break
                }
            }
            self?.popupAlert.removeFromSuperview()
            self?.backgroundView.removeFromSuperview()
        }
    }
    
    func reAccessPhoto() {
        let alertController = UIAlertController(title: Localizable.localizedString("AccessTitle"), message: Localizable.localizedString("AccessMessage"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Localizable.localizedString("AccessGranted"), style: .default) { (_) in
            if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
//                UserDefaultManager.shared.isOpenUserPhotoSettings = true
            }
        }
        
        let cancelAction = UIAlertAction(title: Localizable.localizedString("AccessDenied"), style: .cancel, handler: nil)
        
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func checkAccessCamera() {
        UserDefaultsManager.shared.isShowInter = true
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        
        if cameraAuthorizationStatus == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                if granted {
                    DispatchQueue.main.async { [weak self] in
                        let vc = CameraViewController()
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    DispatchQueue.main.async { [weak self] in
                        let vc = PermissionViewController()
                        vc.isCamera = true
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                UserDefaultsManager.shared.isShowInter = false
            }
        }else if cameraAuthorizationStatus == .authorized{
            let vc = CameraViewController()
            navigationController?.pushViewController(vc, animated: true)
            UserDefaultsManager.shared.isShowInter = false
        } else if cameraAuthorizationStatus == .denied || cameraAuthorizationStatus == .restricted{
            DispatchQueue.main.async { [weak self] in
                let vc = PermissionViewController()
                vc.isCamera = true
                self?.navigationController?.pushViewController(vc, animated: true)
                UserDefaultsManager.shared.isShowInter = false
            }
        }
        popupAlert.removeFromSuperview()
        backgroundView.removeFromSuperview()
    }
    
    func reAccessCamera() {
        let alertController = UIAlertController(title: Localizable.localizedString("TitleAlertAccessCamera"), message: Localizable.localizedString("MessAlertAccessCamera"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Localizable.localizedString("OKAccessCamera"), style: .default) { (_) in
            if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
                UserDefaultsManager.shared.isOpenSettings = true
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: Localizable.localizedString("CancelAccessCamera"), style: .cancel, handler: nil)
        
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func openFilePicker() {
//        let documentPicker = UIDocumentPickerViewController(forOpeningContentTypes: [.pdf])
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.content"], in: .import)
        documentPicker.delegate = self
        popupAlert.removeFromSuperview()
        backgroundView.removeFromSuperview()
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        // Handle the selected file(s) here
        for url in urls {
            // Do something with the selected file URL
            performNavigation(url: url)
            UserDefaultsManager.shared.countShowRating += 1
        }
    }
    
    func bytesToMegabytes(bytes: Int64) -> Double {
        let megabyte = 1024 * 1024 // 1 MB = 1024 KB = 1024 * 1024 bytes
        return Double(bytes) / Double(megabyte)
    }
    
    func getPdfFileSize(filePath: String) -> Int64? {
        do {
            // Get the file attributes
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: filePath)
            
            // Extract the file size
            if let fileSize = fileAttributes[FileAttributeKey.size] as? Int64 {
                return fileSize
            }
        } catch {
            // Handle any errors that occur during the process
            print("Error: \(error)")
        }
        
        // Return nil if the file size couldn't be retrieved
        return nil
    }
    
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        // Handle document picker cancellation (if needed)
    }
    
    func performNavigation(url: URL){
        DispatchQueue.main.async {
            if self.isPDFFile(fileURL: url) {
                if let pdfDocument = PDFDocument(url: url) {
                    let pdfViewController = PDFViewController()
                    pdfViewController.pdfURL = url.absoluteString
                    let pdfFileName = url.lastPathComponent
                    pdfViewController.pdfName = pdfFileName
                    pdfViewController.pdfDocument = pdfDocument
                    UserDefaultsManager.shared.currentPDF = pdfFileName
                    self.navigationController?.pushViewController(pdfViewController, animated: true)
                    if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        let appUrl = documentsDirectory.appendingPathComponent(pdfFileName)
                        if !self.pdfFileAlreadySaved(url: appUrl.absoluteString, fileName: pdfFileName) {
                            self.savePdf(urlString: url.absoluteString, fileName: pdfFileName)
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd/MM/yyyy"
                            let currentDate = Date()
                            let formattedDate = dateFormatter.string(from: currentDate)
                            let fileSize = self.getPdfFileSize(filePath: url.path)
                            let formatSize = self.bytesToMegabytes(bytes: fileSize!)
                            if self.isPDFFile(fileURL: url) {
                                self.data.insert(PDFData(url: appUrl.absoluteString, name: pdfFileName, date: formattedDate, size: "\(String(format: "%.2f", formatSize)) MB", isFavorite: false), at: 0)
                            }
                        } else {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd/MM/yyyy"
                            let currentDate = Date()
                            let formattedDate = dateFormatter.string(from: currentDate)
                            let fileSize = self.getPdfFileSize(filePath: url.path)
                            let formatSize = self.bytesToMegabytes(bytes: fileSize!)
                            let pdfData = PDFData(url: appUrl.absoluteString, name: pdfFileName, date: formattedDate, size: "\(String(format: "%.2f", formatSize)) MB", isFavorite: false)
                            if !self.data.contains(where: { $0.url == appUrl.absoluteString }) {
                                self.data.insert(pdfData, at: 0)
                            }
                        }
                    }
                    UserDefaultsManager.shared.savePDF(listPDF: self.data)
                    self.setupData()
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }
            } else {
                self.setupFailedPopup()
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.removeFailedPopup()
                }
            }
        }
    }
    
    func setupDeletePop() {
        backgroundView.frame = view.bounds
        backgroundView.backgroundColor = .systemGray
        backgroundView.layer.opacity = 0.8
        view.addSubview(backgroundView)
        customPopup.layer.cornerRadius = 12
        customPopup.layer.masksToBounds = true
        customPopup.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(customPopup)
        NSLayoutConstraint.activate([
            customPopup.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1),
            customPopup.centerYAnchor.constraint(equalToSystemSpacingBelow: view.centerYAnchor, multiplier: 0.7),
            customPopup.heightAnchor.constraint(equalToConstant: 204),
            customPopup.widthAnchor.constraint(equalToConstant: 334),
        ])
        if UserDefaultsManager.shared.isiPad() {
            customPopup.heightAnchor.constraint(equalToConstant: 320).isActive = true
            customPopup.widthAnchor.constraint(equalToConstant: 576).isActive = true
        }
        customPopup.cancelLbl.text = Localizable.localizedString("CancelBtn").uppercased()
        customPopup.confirmLbl.text = Localizable.localizedString("DeleteBtn").uppercased()
        customPopup.jumpLbl.text = Localizable.localizedString("DeleteTitle")
        customPopup.cancelBtn.layer.cornerRadius = 8
        customPopup.confirmBtn.layer.cornerRadius = 8
        customPopup.txtField.isHidden = true
        customPopup.cancelBtn.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        customPopup.confirmBtn.removeTarget(nil, action: nil, for: .allEvents)
        customPopup.confirmBtn.addTarget(self, action: #selector(deleteFile), for: .touchUpInside)
        customPopup.invalidText.text = Localizable.localizedString("Invalid")
        customPopup.deleteLbl.text = Localizable.localizedString("DeleteNotice")
        customPopup.invalidText.isHidden = true
    }
    
    func setupRenamePop() {
        backgroundView.frame = view.bounds
        backgroundView.backgroundColor = .systemGray
        backgroundView.layer.opacity = 0.8
        view.addSubview(backgroundView)
        customPopup.layer.cornerRadius = 12
        customPopup.layer.masksToBounds = true
        customPopup.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(customPopup)
        NSLayoutConstraint.activate([
            customPopup.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1),
            customPopup.centerYAnchor.constraint(equalToSystemSpacingBelow: view.centerYAnchor, multiplier: 0.7),
            customPopup.heightAnchor.constraint(equalToConstant: 204),
            customPopup.widthAnchor.constraint(equalToConstant: 334),
        ])
        if UserDefaultsManager.shared.isiPad() {
            customPopup.heightAnchor.constraint(equalToConstant: 320).isActive = true
            customPopup.widthAnchor.constraint(equalToConstant: 576).isActive = true
        }
        txtField.resignFirstResponder()
        customPopup.txtField.becomeFirstResponder()
        customPopup.cancelLbl.text = Localizable.localizedString("CancelBtn").uppercased()
        customPopup.confirmLbl.text = Localizable.localizedString("SaveBtn").uppercased()
        customPopup.jumpLbl.text = Localizable.localizedString("RenameTitle")
        customPopup.cancelBtn.layer.cornerRadius = 8
        customPopup.confirmBtn.layer.cornerRadius = 8
        customPopup.txtField.isHidden = false
        customPopup.txtField.attributedPlaceholder = NSAttributedString(
                string: Localizable.localizedString("RenamePlaceholder"),
                attributes: [
                    .foregroundColor: UIColor(named: "PlaceholderColor") ?? UIColor.lightGray
                ]
            )
        customPopup.cancelBtn.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        customPopup.confirmBtn.removeTarget(nil, action: nil, for: .allEvents)
        customPopup.confirmBtn.addTarget(self, action: #selector(renameFile), for: .touchUpInside)
        customPopup.invalidText.text = Localizable.localizedString("InvalidName")
        customPopup.invalidText.isHidden = true
    }
    
    @objc func cancel() {
        backgroundView.removeFromSuperview()
        customPopup.removeFromSuperview()
    }
    
    @objc func deleteFile() {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURLToDelete = documentsDirectory.appendingPathComponent(filterData[currentFile!].name)
            do {
                try FileManager.default.removeItem(at: fileURLToDelete)
                if UserDefaultsManager.shared.currentPDF == filterData[currentFile!].name {
                    UserDefaultsManager.shared.currentPDF = ""
                }
                print("File deleted successfully.")
            } catch {
                print("Error deleting file: \(error)")
            }
        } else {
            print("Documents directory not found.")
        }
        for (index, item) in data.enumerated() {
            if item.name == filterData[currentFile!].name {
                data.remove(at: index)
            }
        }
        filterData.remove(at: currentFile!)
        UserDefaultsManager.shared.savePDF(listPDF: data)
        if isSearching {
            if let text = self.txtField.text {
                if text == "" {
                    setupData()
                } else {
                    filterData = data.filter { $0.name.lowercased().contains(text.lowercased()) }
                }
            }
        } else {
            setupData()
        }
        tableView.reloadData()
        if !isSearching {
            setupView()
        }
        backgroundView.removeFromSuperview()
        customPopup.removeFromSuperview()
        if filterData.count < 2 {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Item"))"
        } else {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Items"))"
        }
    }
    
    func renamePDFFile(oldFileName: String, newFileName: String) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let oldFileURL = documentsDirectory.appendingPathComponent(oldFileName)
            let newFileURL = documentsDirectory.appendingPathComponent(newFileName)
            
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for content in contents {
                    if URL(string: content.description)!.lastPathComponent == newFileName {
                        isDuplicate = true
                    }
                }
                if !isDuplicate {
                    try FileManager.default.moveItem(at: oldFileURL, to: newFileURL)
                }
                if UserDefaultsManager.shared.currentPDF == oldFileName {
                    UserDefaultsManager.shared.currentPDF = newFileName
                }
            } catch {
                print("Error renaming PDF file: \(error)")
            }
        }
    }
    
    @objc func renameFile() {
        let currentName = filterData[currentFile!].name
        if let nameChange = customPopup.txtField.text {
            if nameChange != "" {
                renamePDFFile(oldFileName: currentName, newFileName: ("\(nameChange)" + ".pdf"))
                if !isDuplicate {
                    for (index, item) in data.enumerated() {
                        if item.name == filterData[currentFile!].name {
                            data[index].name = ("\(nameChange)" + ".pdf")
                        }
                    }
                    UserDefaultsManager.shared.savePDF(listPDF: data)
                    backgroundView.removeFromSuperview()
                    customPopup.removeFromSuperview()
                } else {
                    customPopup.invalidText.text = Localizable.localizedString("InvalidName")
                    customPopup.invalidText.isHidden = false
                }
                if isSearching {
                    if txtField.text == "" {
                        setupData()
                    } else {
                        filterData = data.filter { $0.name.lowercased().contains(txtField.text?.lowercased() ?? "") }
                    }
                } else {
                    setupData()
                }
                if filterData.count < 2 {
                    numberItems.text = "\(filterData.count) \(Localizable.localizedString("Item"))"
                } else {
                    numberItems.text = "\(filterData.count) \(Localizable.localizedString("Items"))"
                }
                tableView.reloadData()
            }  else {
                customPopup.invalidText.text = Localizable.localizedString("Invalid")
                customPopup.invalidText.isHidden = false
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.customPopup.invalidText.isHidden = true
        }
        customPopup.txtField.text = ""
        customPopup.txtField.placeholder = Localizable.localizedString("RenamePlaceholder")
        isDuplicate = false
    }
    
    @IBAction func toggleSearching(_ sender: Any) {
        isSearching = true
        txtField.becomeFirstResponder()
        setupView()
    }
    
    @IBAction func cancelSearching(_ sender: Any) {
        isSearching = false
        txtField.text = ""
        setupData()
        if filterData.count < 2 {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Item"))"
        } else {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Items"))"
        }
        view.endEditing(true)
        tableView.reloadData()
        setupView()
    }
    
    func setupFailedPopup() {
        backgroundView.frame = view.bounds
        backgroundView.backgroundColor = .systemGray
        backgroundView.layer.opacity = 0.8
        view.addSubview(backgroundView)
        popupFailed.layer.cornerRadius = 12
        popupFailed.layer.masksToBounds = true
        popupFailed.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(popupFailed)
        NSLayoutConstraint.activate([
            popupFailed.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1),
            popupFailed.centerYAnchor.constraint(equalToSystemSpacingBelow: view.centerYAnchor, multiplier: 0.7),
            popupFailed.heightAnchor.constraint(equalToConstant: 230),
            popupFailed.widthAnchor.constraint(equalToConstant: 250),
        ])
        
        ecgAnimatedView = .init(name: "upload failed")
        ecgAnimatedView.translatesAutoresizingMaskIntoConstraints = false
        popupFailed.loadingAnimation.addSubview(ecgAnimatedView)
        NSLayoutConstraint.activate([
            ecgAnimatedView.topAnchor.constraint(equalTo: popupFailed.loadingAnimation.topAnchor),
            ecgAnimatedView.bottomAnchor.constraint(equalTo: popupFailed.loadingAnimation.bottomAnchor),
            ecgAnimatedView.leadingAnchor.constraint(equalTo: popupFailed.loadingAnimation.leadingAnchor),
            ecgAnimatedView.trailingAnchor.constraint(equalTo: popupFailed.loadingAnimation.trailingAnchor)
        ])
        ecgAnimatedView.contentMode = .scaleAspectFill
        ecgAnimatedView.loopMode = .loop
        ecgAnimatedView.animationSpeed = 1
        ecgAnimatedView.play()
        popupFailed.failedLbl.text = Localizable.localizedString("FailedPopup")
        removeTapGesture = UITapGestureRecognizer(target: self, action: #selector(removeFailedPopup))
        backgroundView.addGestureRecognizer(removeTapGesture!)
    }
    
    @objc func removeFailedPopup() {
        popupFailed.removeFromSuperview()
        backgroundView.removeGestureRecognizer(removeTapGesture!)
        removeTapGesture?.cancelsTouchesInView = false
        backgroundView.removeFromSuperview()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("DismissKeyboard"), object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0 && filterData[indexPath.row].name == "Example PDF.pdf"{
//            let vc = PDFViewController()
//            guard let pdfURL = Bundle.main.url(forResource: "Example PDF", withExtension: "pdf") else { return }
//            vc.pdfDocument = PDFDocument(url: pdfURL)
//            vc.pdfURL = "Example PDF.pdf"
//            vc.pdfName = "Example PDF.pdf"
//            UserDefaultsManager.shared.currentPDF = "Example PDF.pdf"
//            navigationController?.pushViewController(vc, animated: true)
//        } else {
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let url = documentsDirectory.appendingPathComponent(filterData[indexPath.row].name)
                showSavedPdf(url: url.absoluteString, fileName: filterData[indexPath.row].name)
            }
//        }
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UserDefaultsManager.shared.isiPad() {
            return 97
        } else {
            return 72
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.pdfName.text = filterData[indexPath.row].name
        cell.pdfDate.text = filterData[indexPath.row].date
        cell.pdfSize.text = filterData[indexPath.row].size
        cell.delegate = self
        cell.cellNumber = indexPath.row
        cell.setupButton()
        if filterData[indexPath.row].isFavorite {
            cell.favoriteBtn.setImage(UIImage(named: "favorite_on"), for: .normal)
        } else {
            cell.favoriteBtn.setImage(UIImage(named: "favorite_unselected"), for: .normal)
        }
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let url = documentsDirectory.appendingPathComponent(filterData[indexPath.row].name)
            cell.url = url.absoluteString
        }
//        print(filterData[indexPath.row].name)
//        if indexPath.row == 0 && filterData[indexPath.row].name == "Example PDF.pdf"{
//            cell.favoriteBtn.isHidden = true
//            cell.optionBtn.isHidden = true
//        } else {
//            cell.favoriteBtn.isHidden = false
//            cell.optionBtn.isHidden = false
//        }
        return cell
    }
}

extension HomeViewController: PDFActionDelegate {
    func optionTapped(_ number: Int,_ type: String,_ url: String,_ cell: UITableViewCell) {
        if type == "share" {
            let activityVC = UIActivityViewController(activityItems: [URL(string: url)!], applicationActivities: nil)
            if UserDefaultsManager.shared.isiPad() {
                if activityVC.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                    activityVC.popoverPresentationController?.sourceView = cell as UIView
                }
            }
            present(activityVC, animated: true, completion: nil)
        } else if type == "delete" {
            currentFile = number
            setupDeletePop()
        } else {
            currentFile = number
            setupRenamePop()
        }
    }
    
    func favoriteTapped(_ number: Int) {
        for (index, item) in data.enumerated() {
            if item.name == filterData[number].name {
                data[index].isFavorite.toggle()
            }
        }
        UserDefaultsManager.shared.savePDF(listPDF: data)
        if isSearching {
            if txtField.text == "" {
                setupData()
            } else {
                filterData = data.filter { $0.name.lowercased().contains(txtField.text?.lowercased() ?? "") }
            }
        } else {
            setupData()
        }
        tableView.reloadData()
    }
    
}

extension HomeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Combine the existing text and the newly entered text
        if let oldString = textField.text {
            let newString = oldString.replacingCharacters(in: Range(range, in: oldString)!, with: string)
            if newString == "" {
                filterData = data
            } else {
                filterData = data.filter { $0.name.lowercased().contains(newString.lowercased()) }

            }
        }
        if filterData.count < 2 {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Item"))"
        } else {
            numberItems.text = "\(filterData.count) \(Localizable.localizedString("Items"))"
        }
        tableView.reloadData()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tapGesture?.isEnabled = true
        tapGesture?.cancelsTouchesInView = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    
}

extension HomeViewController: ImportDelegate {
    func chooseType(type: CurrentChoose) {
        switch type {
        case .none:
            break
        case .photo:
            requestPhotoLibraryPermission()
        case .camera:
            checkAccessCamera()
        case .file:
            openFilePicker()
        }
    }
    
    
}
