//
//  HomeTableViewCell.swift
//  PDFReader
//
//  Created by DEV on 22/09/2023.
//

import UIKit

protocol PDFActionDelegate {
    func optionTapped(_ number: Int,_ type: String,_ url: String,_ cell: UITableViewCell)
    func favoriteTapped(_ number: Int)
}

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var pdfName: UILabel!
    @IBOutlet weak var pdfDate: UILabel!
    @IBOutlet weak var pdfSize: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var optionBtn: UIButton!
    
    var delegate: PDFActionDelegate?
    var cellNumber: Int = 0
    var url: String!
    var items: [UIMenuElement]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        if UserDefaultsManager.shared.isiPad() {
            pdfName.font = UIFont(name: "Inter-Bold", size: 17)
            pdfDate.font = UIFont(name: "Inter", size: 14)
            pdfSize.font = UIFont(name: "Inter", size: 14)
            favoriteBtn.heightAnchor.constraint(equalToConstant: 32).isActive = true
            favoriteBtn.widthAnchor.constraint(equalToConstant: 32).isActive = true
            optionBtn.heightAnchor.constraint(equalToConstant: 32).isActive = true
            optionBtn.widthAnchor.constraint(equalToConstant: 32).isActive = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func option(_ sender: Any) {
        setupButton()
    }
    
    @IBAction func favorite(_ sender: Any) {
        delegate?.favoriteTapped(cellNumber)
    }
    
    func setupButton() {
        let sharePDF = UIImage(named: "home_share")
        let deletePDF = UIImage(named: "home_delete")
        let renamePDF = UIImage(named: "home_rename")
        let shareAction = UIAction(title: Localizable.localizedString("MenuShare"), image: sharePDF) { [weak self] action in
            
            if let self = self {
                if let delegate = self.delegate {
                    delegate.optionTapped(cellNumber, "share", self.url, self)
                }
            }
            
        }
        let deleteAction = UIAction(title: Localizable.localizedString("MenuDelete"), image: deletePDF) {[weak self] action in
            if let self = self {
                if let delegate = self.delegate {
                    delegate.optionTapped(cellNumber, "delete", url!, self)
                }
            }
            
        }
        
        let renameAction = UIAction(title: Localizable.localizedString("MenuRename"), image: renamePDF) {[weak self] action in
            if let self = self {
                if let delegate = self.delegate {
                    delegate.optionTapped(cellNumber, "rename", url!, self)
                }
            }
        }
        items = [renameAction, shareAction, deleteAction]
        optionBtn.menu = UIMenu(title: "", children: items!)
        optionBtn.showsMenuAsPrimaryAction = true
    }
}
