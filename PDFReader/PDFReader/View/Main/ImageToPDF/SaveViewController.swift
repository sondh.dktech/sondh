//
//  SaveViewController.swift
//  PDFReader
//
//  Created by DEV on 07/12/2023.
//

import UIKit
import Lottie
import PDFKit

class SaveViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var viewFileBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var bannerView: UIView!
    
    var pdfDocument: PDFDocument?
    var pdfName: String?
    var pdfURL: URL?
    var ecgAnimatedView: LottieAnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAnimation()
        titleLbl.text = Localizable.localizedString("SaveImage")
        setupBanner()
        // Do any additional setup after loading the view.
    }
    
    func setupBanner() {
        let id = AdMobConstants.BANNER_COLLAPSE_SUCCESSFULLY
        let config = BannerPlugin.Config(defaultAdUnitId: id, defaultBannerType: .CollapsibleBottom)
        let _ = BannerPlugin(rootViewController: self, adContainer: bannerView, config: config)
    }
    
    @IBAction func viewFileTapped(_ sender: Any) {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            guard let pdfName = pdfName else { return }
            let url = documentsDirectory.appendingPathComponent(pdfName)
            showSavedPdf(url: url.absoluteString, fileName: pdfName)
        }
    }
    
    @IBAction func backHomeTapped(_ sender: Any) {
        navigationController?.popToRootViewController(animated: false)
    }
    
    func showSavedPdf(url:String, fileName:String) {
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for content in contents {
                    if URL(string: content.description)!.lastPathComponent == fileName {
                        let vc = PDFViewController()
                        vc.pdfDocument = PDFDocument(url: URL(string: content.absoluteString)!)
                        vc.pdfURL = content.absoluteString
                        vc.pdfName = fileName
                        UserDefaultsManager.shared.currentPDF = fileName
                        navigationController?.pushViewController(vc, animated: true)
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
    }
    
    func setupAnimation() {
        viewFileBtn.setTitle(Localizable.localizedString("ViewFile"), for: .normal)
        viewFileBtn.layer.borderColor = UIColor(rgb: 0xCC1515).cgColor
        viewFileBtn.layer.borderWidth = 1
        viewFileBtn.layer.cornerRadius = 10
        viewFileBtn.layer.masksToBounds = true
        backBtn.setTitle(Localizable.localizedString("BackHome"), for: .normal)
        backBtn.layer.cornerRadius = 10
        backBtn.layer.masksToBounds = true
        ecgAnimatedView = .init(name: "Completed")
        ecgAnimatedView.translatesAutoresizingMaskIntoConstraints = false
        animationView.addSubview(ecgAnimatedView)
        NSLayoutConstraint.activate([
            ecgAnimatedView.topAnchor.constraint(equalTo: animationView.topAnchor),
            ecgAnimatedView.bottomAnchor.constraint(equalTo: animationView.bottomAnchor),
            ecgAnimatedView.leadingAnchor.constraint(equalTo: animationView.leadingAnchor),
            ecgAnimatedView.trailingAnchor.constraint(equalTo: animationView.trailingAnchor)
        ])
        ecgAnimatedView.contentMode = .scaleAspectFit
        ecgAnimatedView.loopMode = .playOnce
        ecgAnimatedView.animationSpeed = 1
        ecgAnimatedView.play()
    }
    
    


}
