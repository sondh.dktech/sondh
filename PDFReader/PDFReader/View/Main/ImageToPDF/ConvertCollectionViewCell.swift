//
//  ConvertCollectionViewCell.swift
//  PDFReader
//
//  Created by DEV on 06/12/2023.
//

import UIKit

protocol ConvertDelegate {
    func select(number: Int)
}

class ConvertCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var tickImage: UIImageView!
    
    var isChoose: Bool = false
    var isHideNumber: Bool = false
    var delegate: ConvertDelegate?
    var index: Int = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        numberLbl.layer.cornerRadius = 10
        numberLbl.layer.masksToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
    }
    
    
    @IBAction func selectTapped(_ sender: Any) {
        delegate?.select(number: index)
    }
    
    func setupCell() {
        if isChoose {
            tickImage.image = UIImage(named: "tick-circle")
        } else {
            tickImage.image = UIImage(named: "untick_circle")
        }
        if isHideNumber {
            numberLbl.isHidden = true
            tickImage.isHidden = false
        } else {
            numberLbl.isHidden = false
            tickImage.isHidden = true
        }
    }
    

}
