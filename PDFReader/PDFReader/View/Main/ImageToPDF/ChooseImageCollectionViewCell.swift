//
//  ChooseImageCollectionViewCell.swift
//  PDFReader
//
//  Created by DEV on 05/12/2023.
//

import UIKit

protocol ChooseImageCollectionDelegate {
    func delete(index: Int, imageIndex: Int)
}

class ChooseImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var delegate: ChooseImageCollectionDelegate?
    var index: Int = -1
    var imageIndex: Int = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.layer.cornerRadius = 10
    }

    @IBAction func deleteTapped(_ sender: Any) {
        delegate?.delete(index: index, imageIndex: imageIndex)
    }
}
