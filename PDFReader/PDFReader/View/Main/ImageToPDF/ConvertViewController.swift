//
//  ConvertViewController.swift
//  PDFReader
//
//  Created by DEV on 06/12/2023.
//

import UIKit
import PDFKit
import AVFoundation
import Photos

class ConvertViewController: UIViewController {

    @IBOutlet weak var convertBtn: UIButton!
    @IBOutlet weak var instructionView: UIView!
    @IBOutlet weak var instructionLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var numberSelectLbl: UILabel!
    @IBOutlet weak var missingLbl1: UILabel!
    @IBOutlet weak var missingLbl2: UILabel!
    @IBOutlet weak var importBtn: UIButton!
    @IBOutlet weak var missingView: UIView!
    @IBOutlet weak var deleteLbl: UILabel!
    
    var data: [PDFData] = []
    private var instruction0: NSLayoutConstraint?
    private var instruction40: NSLayoutConstraint?
    var isDeleting: Bool = false
    var backgroundView = UIView()
    var customPopup = PopupCollectionView()
    var arrayImage: [ImageConvert] = []
    var count: Int = 0
    var longPressGesture: UILongPressGestureRecognizer!
    var popupAlert = PopupChooseFile()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        instruction0 = instructionView.heightAnchor.constraint(equalToConstant: 0)
        instruction40 = instructionView.heightAnchor.constraint(equalToConstant: 40)
        setupView()
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isDeleting = false
        count = 0
        checkExist()
        setupView()
        setupData()
    }
    
    func setupAlert() {
        backgroundView.frame = view.bounds
        backgroundView.backgroundColor = .systemGray
        backgroundView.layer.opacity = 0.8
        view.addSubview(backgroundView)
        popupAlert.layer.cornerRadius = 12
        popupAlert.layer.masksToBounds = true
        popupAlert.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(popupAlert)
        NSLayoutConstraint.activate([
            popupAlert.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            popupAlert.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            popupAlert.heightAnchor.constraint(equalToConstant: 224),
            popupAlert.widthAnchor.constraint(equalToConstant: 300),
        ])
        popupAlert.photoLbl.text = Localizable.localizedString("PhotosOption")
        popupAlert.titleLbl.text = Localizable.localizedString("SourceTitle")
        popupAlert.cameraLbl.text = Localizable.localizedString("CameraOption")
        popupAlert.fileView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        popupAlert.fileView.isHidden = true
        popupAlert.cancelLbl.text = Localizable.localizedString("CancelAccessCamera")
        popupAlert.photoBtn.addTarget(self, action: #selector(requestPhotoLibraryPermission), for: .touchUpInside)
        popupAlert.cameraBtn.addTarget(self, action: #selector(checkAccessCamera), for: .touchUpInside)
        popupAlert.cancelBtn.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
    }
    
    @objc func requestPhotoLibraryPermission() {
        let accessLevel: PHAccessLevel = .readWrite
        DispatchQueue.main.async { [weak self] in
            PHPhotoLibrary.requestAuthorization(for: accessLevel) { [weak self] status in
                switch status {
                case .limited:
                    DispatchQueue.main.async { [weak self] in
                        let vc = LibraryViewController()
                        vc.isConverting = true
                        vc.delegate = self
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                case .authorized:
                    DispatchQueue.main.async { [weak self] in
                        let vc = LibraryViewController()
                        vc.isConverting = true
                        vc.delegate = self
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                case .denied, .restricted:
                    DispatchQueue.main.async { [weak self] in
                        let vc = PermissionViewController()
                        vc.isCamera = false
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                @unknown default:
                    break
                }
            }
            self?.popupAlert.removeFromSuperview()
            self?.backgroundView.removeFromSuperview()
        }
    }
    
    @objc func closePopup() {
        popupAlert.removeFromSuperview()
        backgroundView.removeFromSuperview()
    }
    
    @objc func checkAccessCamera() {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        
        if cameraAuthorizationStatus == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                if granted {
                    DispatchQueue.main.async {
                        let vc = CameraViewController()
                        vc.isConverting = true
                        vc.delegate = self
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    DispatchQueue.main.async {
                        let vc = PermissionViewController()
                        vc.isCamera = true
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }else if cameraAuthorizationStatus == .authorized{
            let vc = CameraViewController()
            vc.delegate = self
            vc.isConverting = true
            navigationController?.pushViewController(vc, animated: true)
        } else if cameraAuthorizationStatus == .denied || cameraAuthorizationStatus == .restricted{
            DispatchQueue.main.async { [weak self] in
                let vc = PermissionViewController()
                vc.isCamera = true
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        popupAlert.removeFromSuperview()
        backgroundView.removeFromSuperview()
    }
    
    func setupData() {
        data = UserDefaultsManager.shared.getListPDF()
    }
    
    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ConvertCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ConvertCollectionViewCell")
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        collectionView.addGestureRecognizer(longPressGesture)

    }
    
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            guard let indexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else {
                break
            }
            collectionView.beginInteractiveMovementForItem(at: indexPath)

        case .changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: collectionView))

        case .ended:
            collectionView.endInteractiveMovement()
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }

        default:
            // Reset the sourceIndexPath and remove the snapshot on cancel
            collectionView.cancelInteractiveMovement()
        }
    }
    
    func setupView() {
        numberSelectLbl.isHidden = true
        instructionView.layer.borderWidth = 1
        missingView.backgroundColor = UIColor(rgb: 0xFAFAFA)
        view.backgroundColor = UIColor(rgb: 0xFAFAFA)
        instructionView.layer.borderColor = UIColor(rgb: 0x91D5FF).cgColor
        deleteLbl.text = Localizable.localizedString("DeleteBtn")
        instructionLbl.text = Localizable.localizedString("MoveItemText")
        convertBtn.setTitle(Localizable.localizedString("NextText"), for: .normal)
        missingLbl1.text = Localizable.localizedString("NoHomeFile1_Des")
        missingLbl2.text = Localizable.localizedString("NoHomeFile2_Des")
        importBtn.setTitle(Localizable.localizedString("Import"), for: .normal)
        convertBtn.layer.cornerRadius = 10
        convertBtn.layer.masksToBounds = true
        importBtn.layer.cornerRadius = 10
        importBtn.layer.masksToBounds = true
    }
    
    func checkExist() {
        isDeleting = false
        if arrayImage.isEmpty {
            collectionView.isHidden = true
            convertBtn.isHidden = true
        } else {
            longPressGesture.isEnabled = true
            instruction0?.priority = UILayoutPriority(998)
            instruction40?.priority = UILayoutPriority(1000)
            instruction0?.isActive = false
            instruction40?.isActive = true
            instructionView.isHidden = false
            for i in 0..<arrayImage.count {
                arrayImage[i].isHiddenNumber = false
                arrayImage[i].isChoose = false
            }
            collectionView.isHidden = false
            convertBtn.isHidden = false
            collectionView.reloadData()
        }
    }
    
    
    @IBAction func convertTapped(_ sender: Any) {
        let pdfDocument = PDFDocument()
        for (key, value) in arrayImage.enumerated() {
            let pdfPage = PDFPage(image: value.image)
            pdfDocument.insert(pdfPage!, at: key)
        }
        let pdfVC = PreviewViewController()
        pdfVC.pdfDocument = pdfDocument
        self.navigationController?.pushViewController(pdfVC, animated: true)
    }
    
    func isPDFFile(fileURL: URL) -> Bool {
        // Get the file's path extension (e.g., "pdf" from "example.pdf")
        do {
            let pdfData = try Data(contentsOf: fileURL)
            // Use the pdfData as needed
            if pdfData.isPDF {
                return true
            } else {
                return false
            }
        } catch {
            print("Error loading PDF data: \(error.localizedDescription)")
            return false
        }
    }
    
    func getPdfFileSize(filePath: String) -> Int64? {
        do {
            // Get the file attributes
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: filePath)
            
            // Extract the file size
            if let fileSize = fileAttributes[FileAttributeKey.size] as? Int64 {
                return fileSize
            }
        } catch {
            // Handle any errors that occur during the process
            print("Error: \(error)")
        }
        
        // Return nil if the file size couldn't be retrieved
        return nil
    }
    
    func bytesToMegabytes(bytes: Int64) -> Double {
        let megabyte = 1024 * 1024 // 1 MB = 1024 KB = 1024 * 1024 bytes
        return Double(bytes) / Double(megabyte)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        if isDeleting && arrayImage.count != 0 {
            isDeleting = false
            longPressGesture.isEnabled = true
            instruction0?.priority = UILayoutPriority(998)
            instruction40?.priority = UILayoutPriority(1000)
            instruction40?.isActive = true
            instruction0?.isActive = false
            instructionView.isHidden = false
            for i in 0..<arrayImage.count {
                arrayImage[i].isHiddenNumber = false
                arrayImage[i].isChoose = false
            }
            count = 0
            numberSelectLbl.isHidden = true
            collectionView.reloadData()
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        if !isDeleting {
            isDeleting = true
            longPressGesture.isEnabled = false
            instruction0?.priority = UILayoutPriority(1000)
            instruction40?.priority = UILayoutPriority(998)
            instruction0?.isActive = true
            instruction40?.isActive = false
            instructionView.isHidden = true
            for i in 0..<arrayImage.count {
                arrayImage[i].isHiddenNumber = true
            }
            collectionView.reloadData()
        } else {
            if count == 0 {
                showAlert(message: Localizable.localizedString("LibraryTitle"))
            } else {
                setupDeletePop()
            }
        }
    }
    
    func setupDeletePop() {
        backgroundView.frame = view.bounds
        backgroundView.backgroundColor = .systemGray
        backgroundView.layer.opacity = 0.8
        view.addSubview(backgroundView)
        customPopup.layer.cornerRadius = 12
        customPopup.layer.masksToBounds = true
        customPopup.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(customPopup)
        NSLayoutConstraint.activate([
            customPopup.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1),
            customPopup.centerYAnchor.constraint(equalToSystemSpacingBelow: view.centerYAnchor, multiplier: 0.7),
            customPopup.heightAnchor.constraint(equalToConstant: 204),
            customPopup.widthAnchor.constraint(equalToConstant: 334),
        ])
        if UserDefaultsManager.shared.isiPad() {
            customPopup.heightAnchor.constraint(equalToConstant: 320).isActive = true
            customPopup.widthAnchor.constraint(equalToConstant: 576).isActive = true
        }
        customPopup.cancelLbl.text = Localizable.localizedString("CancelBtn").uppercased()
        customPopup.confirmLbl.text = Localizable.localizedString("DeleteBtn").uppercased()
        customPopup.jumpLbl.text = Localizable.localizedString("DeleteTitle")
        customPopup.cancelBtn.layer.cornerRadius = 8
        customPopup.confirmBtn.layer.cornerRadius = 8
        customPopup.txtField.isHidden = true
        customPopup.cancelBtn.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        customPopup.confirmBtn.addTarget(self, action: #selector(deleteFile), for: .touchUpInside)
        customPopup.invalidText.text = Localizable.localizedString("Invalid")
        customPopup.deleteLbl.text = Localizable.localizedString("DeleteNotice")
        customPopup.invalidText.isHidden = true
    }
    
    
    @IBAction func importTapped(_ sender: Any) {
        setupAlert()
    }
    
    
    @objc func cancel() {
        backgroundView.removeFromSuperview()
        customPopup.removeFromSuperview()
    }
    
    @objc func deleteFile() {
        let removedCount = arrayImage.removeAll { imageData in
            return imageData.isChoose
        }
        let removedImages = arrayImage.filter { imageData in
            return imageData.isChoose
        }
        count = removedImages.count
        numberSelectLbl.text = Localizable.localizedString("\(count) selected")
        if count == 0 {
            numberSelectLbl.isHidden = true
        }
        collectionView.reloadData()
        checkExist()
        backgroundView.removeFromSuperview()
        customPopup.removeFromSuperview()
    }
}

extension ConvertViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        collectionView.performBatchUpdates({
            // Update your data source to reflect the move
            let item = arrayImage.remove(at: sourceIndexPath.row)
            arrayImage.insert(item, at: destinationIndexPath.row)
            
        }, completion: { finished in
            self.collectionView.reloadData()
        })
//        let item = arrayImage.remove(at: sourceIndexPath.row)
//        arrayImage.insert(item, at: destinationIndexPath.row)
//        print(arrayImage)
//        self.collectionView.reloadData()
    }
}

extension ConvertViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConvertCollectionViewCell", for: indexPath) as! ConvertCollectionViewCell
        cell.imageView.image = arrayImage[indexPath.row].image
        cell.numberLbl.text = "\(indexPath.row+1)"
        cell.isHideNumber = arrayImage[indexPath.row].isHiddenNumber
        cell.isChoose = arrayImage[indexPath.row].isChoose
        cell.index = indexPath.row
        cell.setupCell()
        cell.delegate = self
        return cell
    }
    
    
}

extension ConvertViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width: Double = 0
        var height: Double = 0
        if UIDevice.current.userInterfaceIdiom == .pad {
            width = (view.frame.width - 79.0) / 4
            height = 187.0
        } else {
            width = (view.frame.width - 38.0) / 2
            height = 214.0
        }
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
}

extension ConvertViewController: ConvertDelegate {
    func select(number: Int) {
        if isDeleting {
            arrayImage[number].isChoose.toggle()
            if arrayImage[number].isChoose {
                count += 1
                numberSelectLbl.isHidden = false
                numberSelectLbl.text = "\(count) \(Localizable.localizedString("SelectedText"))"
            } else {
                count -= 1
                numberSelectLbl.text = "\(count) \(Localizable.localizedString("SelectedText"))"
                if count == 0 {
                    numberSelectLbl.isHidden = true
                }
            }
            collectionView.reloadData()
        }
    }
    
}

extension ConvertViewController: CameraDelegate {
    func getCameraData(data: [ImageConvert]) {
        arrayImage = data
        longPressGesture.isEnabled = true
        instruction0?.priority = UILayoutPriority(998)
        instruction40?.priority = UILayoutPriority(1000)
        instruction0?.isActive = false
        instruction40?.isActive = true
        instructionView.isHidden = false
        for i in 0..<arrayImage.count {
            arrayImage[i].isHiddenNumber = false
        }
        collectionView.reloadData()
    }
}

extension ConvertViewController: LibraryDelegate {
    func getData(data: [ImageConvert]) {
        arrayImage = data
        longPressGesture.isEnabled = true
        longPressGesture.isEnabled = true
        instruction0?.priority = UILayoutPriority(998)
        instruction40?.priority = UILayoutPriority(1000)
        instruction0?.isActive = false
        instruction40?.isActive = true
        instructionView.isHidden = false
        for i in 0..<arrayImage.count {
            arrayImage[i].isHiddenNumber = false
        }
        collectionView.reloadData()
    }
    
    
}

extension ConvertViewController {
    func showAlert(title:String = "",
                   message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

