//
//  PreviewViewController.swift
//  PDFReader
//
//  Created by DEV on 07/12/2023.
//

import UIKit
import PDFKit
import MobileCoreServices
import GoogleMobileAds


class PreviewViewController: UIViewController {

    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var convertBtn: UIButton!
    @IBOutlet weak var convertView: UIView!
    
    var interstitial: GADInterstitialAd?
    var data: [PDFData] = []
    var pdfView = PDFView()
    var pdfDocument: PDFDocument?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(pdfView)
        setupBanner()
        setupData()
        convertBtn.setTitle(Localizable.localizedString("SaveFile"), for: .normal)
        titleLbl.text = Localizable.localizedString("PreviewText")
        navigationController?.setNavigationBarHidden(true, animated: true)
        convertBtn.layer.cornerRadius = 10
        convertBtn.layer.masksToBounds = true
        pdfView.backgroundColor = UIColor(rgb: 0xF9F9F9)
        pdfView.document = pdfDocument

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.dismiss(animated: true, completion: nil)
            self.pdfView.maxScaleFactor = 3;
            self.pdfView.minScaleFactor = self.pdfView.scaleFactorForSizeToFit;
            self.pdfView.autoScales = true;
            self.pdfView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
    }
    
    override func viewDidLayoutSubviews() {
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pdfView.topAnchor.constraint(equalTo: backBtn.bottomAnchor, constant: 20),
            pdfView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pdfView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pdfView.bottomAnchor.constraint(equalTo: convertView.topAnchor)
        ])
    }
    func setupBanner() {
        let id = AdMobConstants.BANNER_PREVIEW
        let config = BannerPlugin.Config(defaultAdUnitId: id, defaultBannerType: .Adaptive)
        let _ = BannerPlugin(rootViewController: self, adContainer: bannerView, config: config)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    func setupData() {
        data = UserDefaultsManager.shared.getListPDF()
    }

    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func showInter() {
        interstitial = InterSaveAdsManager.shared.getInter()
        interstitial?.fullScreenContentDelegate = self
        if let interstitial = interstitial {
            interstitial.present(fromRootViewController: self)
            interstitial.paidEventHandler = { [weak self] adValue in
                guard let self = self else { return }
                PaidEventHandlerManager.shared.getPaidEventHandler(dataPaidEvent: adValue, typeAds: .interAds, inter: self.interstitial, adUnit: AdMobConstants.INTER_SAVE)
            }
        } else {
            InterSaveAdsManager.shared.loadInter()
            continueStep()
        }
    }
    
    func continueStep() {
        let vc = SaveViewController()
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let pdfFileName = "\(UUID().uuidString).pdf"
        let pdfFileURL = documentsDirectory.appendingPathComponent(pdfFileName)
        pdfDocument?.write(to: pdfFileURL)
        vc.pdfDocument = pdfDocument
        vc.pdfName = pdfFileName
        vc.pdfURL = pdfFileURL
        navigationController?.pushViewController(vc, animated: true)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let currentDate = Date()
        let formattedDate = dateFormatter.string(from: currentDate)
        let fileSize = self.getPdfFileSize(filePath: pdfFileURL.path)
        let formatSize = self.bytesToMegabytes(bytes: fileSize!)
        if self.isPDFFile(fileURL: pdfFileURL) {
            self.data.append(PDFData(url: pdfFileURL.absoluteString, name: pdfFileName, date: formattedDate, size: "\(String(format: "%.2f", formatSize)) MB", isFavorite: false))
            UserDefaultsManager.shared.savePDF(listPDF: self.data)
        }
    }
    
    @IBAction func convertTapped(_ sender: Any) {
        showInter()
    }
    
    func isPDFFile(fileURL: URL) -> Bool {
        // Get the file's path extension (e.g., "pdf" from "example.pdf")
        do {
            let pdfData = try Data(contentsOf: fileURL)
            // Use the pdfData as needed
            if pdfData.isPDF {
                return true
            } else {
                return false
            }
        } catch {
            print("Error loading PDF data: \(error.localizedDescription)")
            return false
        }
    }
    
    func getPdfFileSize(filePath: String) -> Int64? {
        do {
            // Get the file attributes
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: filePath)
            
            // Extract the file size
            if let fileSize = fileAttributes[FileAttributeKey.size] as? Int64 {
                return fileSize
            }
        } catch {
            // Handle any errors that occur during the process
            print("Error: \(error)")
        }
        
        // Return nil if the file size couldn't be retrieved
        return nil
    }
    
    func bytesToMegabytes(bytes: Int64) -> Double {
        let megabyte = 1024 * 1024 // 1 MB = 1024 KB = 1024 * 1024 bytes
        return Double(bytes) / Double(megabyte)
    }

}

extension PreviewViewController: GADFullScreenContentDelegate {
    func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad will present full screen content.")
        UserDefaultsManager.shared.isShowSplashInter = true
    }
    
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
    {
        continueStep()
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        UserDefaultsManager.shared.isShowSplashInter = false
        InterSaveAdsManager.shared.loadInter()
        continueStep()
    }
}
