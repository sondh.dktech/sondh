//
//  CameraViewController.swift
//  testCoreLocation
//
//  Created by DEV on 08/11/2023.
//

import UIKit
import AVFoundation

protocol CameraDelegate {
    func getCameraData(data: [ImageConvert])
}

class CameraViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var bannerView: UIView!
    
    
    var delegate: CameraDelegate?
    var isConverting: Bool = false
    var bgIndicator : UIView?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var captureSession: AVCaptureSession!
    var videoDeviceInput: AVCaptureDeviceInput!
    var videoDataOutput: AVCaptureVideoDataOutput!
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput!
    var isFriend: Bool = false
    var id: String?
    var flashMode: AVCaptureDevice.FlashMode = .off
    var isFlashing: Bool = false
    var arrImages: [UIImage] = []
    var imageTaken = UIImageView()
    var numberImgLbl = UILabel()
    var captureBtn = UIButton()
    var flashBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = Localizable.localizedString("CameraOption")
//        actionView.backgroundColor = UIColor(hexString: "#313131")
        navigationController?.setNavigationBarHidden(true, animated: true)
        setupCaptureSession()
        setupView()
        setupBanner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        captureBtn.isEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        captureSession.stopRunning()
        turnOffFlash()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startCaptureSession()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        videoPreviewLayer?.frame = cameraView.layer.bounds
        setUpLayoutConstraints()
    }
    
    func setupBanner() {
        let id = AdMobConstants.BANNER_COLLAPSE_CAMERA
        let config = BannerPlugin.Config(defaultAdUnitId: id, defaultBannerType: .CollapsibleBottom)
        let _ = BannerPlugin(rootViewController: self, adContainer: bannerView, config: config)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUpLayoutConstraints() {
        captureBtn.translatesAutoresizingMaskIntoConstraints = false
        captureBtn.widthAnchor.constraint(equalToConstant: 64).isActive = true
        captureBtn.heightAnchor.constraint(equalToConstant: 64).isActive = true
        captureBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        captureBtn.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor, constant: -20).isActive = true
        
        flashBtn.widthAnchor.constraint(equalToConstant: 55).isActive = true
        flashBtn.heightAnchor.constraint(equalToConstant: 55).isActive = true
        NSLayoutConstraint(item: flashBtn, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 0.5, constant: 0).isActive = true
        flashBtn.centerYAnchor.constraint(equalTo: captureBtn.centerYAnchor).isActive = true
        
        imageTaken.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageTaken.heightAnchor.constraint(equalToConstant: 50).isActive = true
        NSLayoutConstraint(item: imageTaken, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.5, constant: 0).isActive = true
        imageTaken.centerYAnchor.constraint(equalTo: captureBtn.centerYAnchor).isActive = true
        
        numberImgLbl.widthAnchor.constraint(equalToConstant: 16).isActive = true
        numberImgLbl.heightAnchor.constraint(equalToConstant: 16).isActive = true
        numberImgLbl.trailingAnchor.constraint(equalTo: imageTaken.trailingAnchor).isActive = true
        numberImgLbl.topAnchor.constraint(equalTo: imageTaken.topAnchor).isActive = true
    }
    
    
    
    func setupView() {
        view.addSubview(captureBtn)
        captureBtn.setImage(UIImage(named: "take_picture"), for: .normal)
        captureBtn.setTitle("", for: .normal)
        captureBtn.addTarget(self, action: #selector(takePhoto), for: .touchUpInside)
        
        view.addSubview(flashBtn)
        flashBtn.setImage(UIImage(named: "Flash"), for: .normal)
        flashBtn.setTitle("", for: .normal)
        flashBtn.addTarget(self, action: #selector(flashTapped), for: .touchUpInside)
        flashBtn.translatesAutoresizingMaskIntoConstraints = false
        
        if UserDefaultsManager.shared.isiPad() {
            flashBtn.isHidden = true
        }

        
        view.addSubview(imageTaken)
        imageTaken.translatesAutoresizingMaskIntoConstraints = false
        imageTaken.layer.cornerRadius = 25
        imageTaken.layer.masksToBounds = true
        imageTaken.contentMode = .scaleAspectFill
        imageTaken.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(convertTapped))
        imageTaken.addGestureRecognizer(tapGesture)
        
        view.addSubview(numberImgLbl)
        numberImgLbl.textColor = .white
        numberImgLbl.translatesAutoresizingMaskIntoConstraints = false
        numberImgLbl.isHidden = true
        numberImgLbl.font = UIFont(name: "Inter", size: 10)
        numberImgLbl.backgroundColor = UIColor(rgb: 0xFF4D4F)
        numberImgLbl.layer.masksToBounds = true
        numberImgLbl.layer.cornerRadius = 8
        numberImgLbl.textAlignment = .center
    }
    
    @objc func convertTapped() {
        if !isConverting {
            let vc = ConvertViewController()
            var images: [ImageConvert] = []
            for image in arrImages {
                images.append(ImageConvert(image: image, isChoose: false, isHiddenNumber: false))
            }
            vc.arrayImage = images
            navigationController?.pushViewController(vc, animated: true)
        } else {
            var images: [ImageConvert] = []
            for image in arrImages {
                images.append(ImageConvert(image: image, isChoose: false, isHiddenNumber: false))
            }
            delegate?.getCameraData(data: images)
            navigationController?.popViewController(animated: false)
        }
    }
    
    func startCaptureSession() {
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            self?.captureSession?.startRunning()
            DispatchQueue.main.async { [weak self] in
//                self?.captureSession?.startRunning()
                self?.captureBtn.isEnabled = true
            }
        }
    }
    
    func setupCaptureSession() {
        captureSession = AVCaptureSession()
        
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .unspecified).devices
        for device in deviceDiscoverySession {
            if device.position == .back {
                backCamera = device
            } else if device.position == .front {
                frontCamera = device
            }
        }
        currentCamera = backCamera
        do {
            videoDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(videoDeviceInput)
            
            photoOutput = AVCapturePhotoOutput()
            captureSession.addOutput(photoOutput)
            
            captureSession.sessionPreset = .high
        } catch {
            print("Error setting up camera input: \(error)")
        }
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = .resizeAspectFill
        let container = UIView(frame: cameraView.bounds)
        container.layer.addSublayer(videoPreviewLayer!)
        cameraView.addSubview(container)
    }
    
//    func toggleCamera() {
//        captureSession.stopRunning()
//        
//        if let currentInput = videoDeviceInput {
//            captureSession.removeInput(currentInput)
//        }
//        
//        if currentCamera == backCamera {
//            currentCamera = frontCamera
//        } else {
//            currentCamera = backCamera
//        }
//        
//        if let newInput = try? AVCaptureDeviceInput(device: currentCamera!) {
//            videoDeviceInput = newInput
//            captureSession.addInput(videoDeviceInput)
//            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
//                self?.captureSession.startRunning()
//            }
//        }
//    }
    
    func capturePhoto() {
        AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
            guard let self = self else { return }

            if granted {
                // User granted access to the camera
                let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
                DispatchQueue.main.async {
                    self.showIndicator()
                }
                self.photoOutput.capturePhoto(with: settings, delegate: self)
            } else {
                // User denied access to the camera
                DispatchQueue.main.async { [weak self] in
                    // Show an alert or perform other actions to inform the user
                    let alertController = UIAlertController(title: Localizable.localizedString("TitleAlertAccessCamera"), message: Localizable.localizedString("MessAlertAccessCamera"), preferredStyle: .alert)
                    
                    let settingsAction = UIAlertAction(title: Localizable.localizedString("OKAccessCamera"), style: .default) { (_) in
                        if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
                            UserDefaultsManager.shared.isOpenCamera = true
                            UIApplication.shared.open(settingsUrl, completionHandler: nil)
                        }
                    }
                    
                    let cancelAction = UIAlertAction(title: Localizable.localizedString("CancelAccessCamera"), style: .cancel, handler: nil)
                    
                    alertController.addAction(settingsAction)
                    alertController.addAction(cancelAction)
                    
                    self?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func showIndicator() {
        bgIndicator = UIView()
        guard let bgIndicator = bgIndicator else {return}
        bgIndicator.frame = view.bounds
        bgIndicator.backgroundColor = .gray.withAlphaComponent(0.5)
        view.addSubview(bgIndicator)
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        indicator.startAnimating()
        indicator.color = .white
        indicator.style = .large
        bgIndicator.addSubview(indicator)
        indicator.center = bgIndicator.center
    }
    
    func removeIndicator() {
        bgIndicator?.removeFromSuperview()
    }
    
    @objc func takePhoto() {
        capturePhoto()
    }
    
    
    @objc func flashTapped() {
        toggleFlash()
    }
    
    func toggleFlash() {
        isFlashing.toggle()
        if isFlashing {
            turnOnFlash()
        } else {
            turnOffFlash()
        }
    }
    
    func turnOnFlash() {
        guard let device = AVCaptureDevice.default(for: .video) else { return }

        do {
            try device.lockForConfiguration()

            if device.isTorchAvailable {
                device.torchMode = .on
            }

            device.unlockForConfiguration()
        } catch {
            print("Failed to lock device for configuration: \(error)")
        }
    }

    func turnOffFlash() {
        guard let device = AVCaptureDevice.default(for: .video) else { return }

        do {
            try device.lockForConfiguration()

            if device.isTorchAvailable {
                device.torchMode = .off
            }

            device.unlockForConfiguration()
        } catch {
            print("Failed to lock device for configuration: \(error)")
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
}

extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {
            print("Error capturing photo: \(error.localizedDescription)")
            return
        }
        if let imageData = photo.fileDataRepresentation(),
           let image = UIImage(data: imageData) {
                imageTaken.image = image
                arrImages.append(image)
                numberImgLbl.text = "\(arrImages.count)"
                numberImgLbl.isHidden = false
                removeIndicator()
        }
    }
}

extension CameraViewController {
    func showAlert(title:String = "",
                   message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
