//
//  LibraryCollectionViewCell.swift
//  testCoreLocation
//
//  Created by DEV on 08/11/2023.
//

import UIKit

protocol LibraryCollectionCellDelegate {
    func select(index: Int, image: UIImage)
    func remove(index: Int)
    func failTapped()
}

class LibraryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tickImg: UIImageView!
    
    var representedAssetIdentifier: String!
    var delegate: LibraryCollectionCellDelegate?
    var isChoose: Bool = false
    var index: Int = -1
    var image: UIImage?
    var isAdded: Bool = false
    var isAvailable: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    func setupCell() {
        if isChoose {
            tickImg.isHidden = false
        } else {
            tickImg.isHidden = true
        }
    }
    
    @IBAction func btnTapped(_ sender: Any) {
        if isAvailable {
            if !isChoose {
                if let image = image {
                    delegate?.select(index: index, image: image)
                }
            } else {
                delegate?.remove(index: index)
            }
        } else {
            delegate?.failTapped()
        }
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }

}
