//
//  LibraryViewController.swift
//  testCoreLocation
//
//  Created by DEV on 08/11/2023.
//

import UIKit
import Photos
import PhotosUI

protocol LibraryDelegate {
    func getData(data: [ImageConvert])
}

class LibraryViewController: UIViewController {
    
    @IBOutlet weak var restrictMessage: UILabel!
    @IBOutlet weak var manageBtn: UIButton!
    @IBOutlet weak var restrictView: UIView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var selectedCollectionView: UICollectionView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var selectLbl: UILabel!
    @IBOutlet weak var nextLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var chooseView: UIView!
    @IBOutlet weak var numberLbl: UILabel!
    
    var isConverting: Bool = false
    var delegate: LibraryDelegate?
    var bgIndicator : UIView?
    let imagePicker = UIImagePickerController()
    var allPhotos : PHFetchResult<PHAsset>? = nil
    var isFriend: Bool = false
    var id: String?
    var imagesArray: [ImageData] = []
    var chooseImage: [Int:UIImage] = [:]
    var arrayNumber: [Int] = []
    private var height0: NSLayoutConstraint?
    private var height60: NSLayoutConstraint?
    var pickerVC: PHPickerViewController?
    fileprivate let imageManager = PHCachingImageManager()
    var fetchResult: PHFetchResult<PHAsset>!
    var assetCollection: PHAssetCollection!
    fileprivate var previousPreheatRect = CGRect.zero
    fileprivate var thumbnailSize: CGSize!
    var imageDict: [Int: ImageData] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resetCachedAssets()
        navigationController?.setNavigationBarHidden(true, animated: true)
        height0 = restrictView.heightAnchor.constraint(equalToConstant: 0)
        height60 = restrictView.heightAnchor.constraint(equalToConstant: 60)
        PHPhotoLibrary.shared().register(self)
        if fetchResult == nil {
            let allPhotosOptions = PHFetchOptions()
            allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            fetchResult = PHAsset.fetchAssets(with: .image, options: allPhotosOptions)
            self.selectLbl.text = "\(Localizable.localizedString("SelectText")) 1-\(self.fetchResult.count) \(Localizable.localizedString("PhotosOption").lowercased())"
            self.numberLbl.text = "(\(self.arrayNumber.count))"
        }
        setupCollectionView()
        requestPhotoLibraryPermission()
        setupView()
        setupBanner()
        manageBtn.layer.cornerRadius = manageBtn.frame.height / 2
        manageBtn.setTitle(Localizable.localizedString("Manage"), for: .normal)
        restrictMessage.text = Localizable.localizedString("Restrict")
    }
    
    
    @IBAction func manage(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let addAction = UIAlertAction(title: Localizable.localizedString("AddPhoto"), style: .default) { [weak self] action in
            guard let self = self else { return }
            PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: self)
        }
        let settingAction = UIAlertAction(title: Localizable.localizedString("ChangeSetting"), style: .default) { [weak self] action in
            if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
                UserDefaultsManager.shared.isOpenLibrary = true
            }
        }
        alert.addAction(addAction)
        alert.addAction(settingAction)
        alert.preferredAction = addAction
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        alert.popoverPresentationController?.permittedArrowDirections = []
        present(alert, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    func showRestrict() {
        if UserDefaultsManager.shared.isFullAccess {
            restrictView.isHidden = true
            self.height0?.priority = UILayoutPriority(1000)
            self.height60?.priority = UILayoutPriority(998)
            height0?.isActive = true
            height60?.isActive = false
            
        } else {
            restrictView.isHidden = false
            self.height60?.priority = UILayoutPriority(1000)
            self.height0?.priority = UILayoutPriority(998)
            height0?.isActive = false
            height60?.isActive = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showRestrict()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        chooseView.layer.cornerRadius = 20
        chooseView.layer.masksToBounds = true
        view.layoutIfNeeded()
    }
    
    fileprivate func updateCachedAssets() {
        // Update only if the view is visible.
        guard isViewLoaded && view.window != nil else { return }
        
        // The window you prepare ahead of time is twice the height of the visible rect.
        let visibleRect = CGRect(origin: collectionView!.contentOffset, size: collectionView!.bounds.size)
        let preheatRect = visibleRect.insetBy(dx: 0, dy: -0.5 * visibleRect.height)
        
        // Update only if the visible area is significantly different from the last preheated area.
        let delta = abs(preheatRect.midY - previousPreheatRect.midY)
        guard delta > view.bounds.height / 3 else { return }
        
        // Compute the assets to start and stop caching.
        let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
        let addedAssets = addedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        let removedAssets = removedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        
        // Update the assets the PHCachingImageManager is caching.
        imageManager.startCachingImages(for: addedAssets,
                                        targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        imageManager.stopCachingImages(for: removedAssets,
                                       targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        // Store the computed rectangle for future comparison.
        previousPreheatRect = preheatRect
    }
    
    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                 width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                 width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                   width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                   width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }
    
    func setupBanner() {
        let id = AdMobConstants.BANNER_PHOTO
        let config = BannerPlugin.Config(defaultAdUnitId: id, defaultBannerType: .Adaptive)
        let _ = BannerPlugin(rootViewController: self, adContainer: bannerView, config: config)
    }
    
    func setupView() {
        titleLbl.text = Localizable.localizedString("PhotosOption")
        backBtn.setTitle(Localizable.localizedString("CancelAccessCamera"), for: .normal)
        nextBtn.layer.cornerRadius = 8
        nextLbl.text = "\(Localizable.localizedString("NextText")) ->"
    }
    
    func setupCollectionView() {
        selectedCollectionView.backgroundColor = .white
        selectedCollectionView.tag = 2
        selectedCollectionView.dataSource = self
        selectedCollectionView.delegate = self
        selectedCollectionView.register(UINib(nibName: "ChooseImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChooseImageCollectionViewCell")
        collectionView.backgroundColor = .white
        collectionView.tag = 1
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "LibraryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LibraryCollectionViewCell")
    }
    
    
    @IBAction func reloadData(_ sender: Any) {
        imageDict.removeAll()
        chooseImage.removeAll()
        arrayNumber.removeAll()
        collectionView.reloadData()
        self.selectedCollectionView.reloadData()
        self.selectLbl.text = "\(Localizable.localizedString("SelectText")) 1-\(self.fetchResult.count) \(Localizable.localizedString("PhotosOption").lowercased())"
        self.numberLbl.text = "(\(arrayNumber.count))"
    }
    
    
    @IBAction func nextTapped(_ sender: Any) {
        if arrayNumber.count == 0 {
            let alert = UIAlertController(title: Localizable.localizedString("LibraryTitle"), message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localizable.localizedString("LibraryDes"), style: .default, handler: nil)
            alert.addAction(okAction)
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = []
            present(alert, animated: true)
        } else {
            if !isConverting {
                let vc = ConvertViewController()
                var images: [ImageConvert] = []
                for number in arrayNumber {
                    if let image = chooseImage[number] {
                        images.append(ImageConvert(image: image, isChoose: false, isHiddenNumber: false))
                    }
                }
                vc.arrayImage = images
                navigationController?.pushViewController(vc, animated: true)
            } else {
                var images: [ImageConvert] = []
                for number in arrayNumber {
                    if let image = chooseImage[number] {
                        images.append(ImageConvert(image: image, isChoose: false, isHiddenNumber: false))
                    }
                }
                delegate?.getData(data: images)
                navigationController?.popViewController(animated: false)
            }
        }
    }
    
    func requestPhotoLibraryPermission() {
        let requiredAccessLevel: PHAccessLevel = .readWrite
        let status = PHPhotoLibrary.authorizationStatus(for: requiredAccessLevel)
        switch status {
        case .limited:
            UserDefaultsManager.shared.isFullAccess = false
            showRestrict()
            //            fetchImagesFromLibrary { [weak self] images in
            //                guard let self = self else { return }
            //                // Use the array of images as needed
            //                for image in images {
            //                    self.imagesArray.append(ImageData(image: image, isChoose: false))
            //                }
            //                self.selectLbl.text = "\(Localizable.localizedString("SelectText")) 1-\(self.imagesArray.count) \(Localizable.localizedString("PhotosOption").lowercased())"
            //                self.collectionView.reloadData()
            //            }
        case .authorized:
            UserDefaultsManager.shared.isFullAccess = true
            showRestrict()
            //            fetchImagesFromLibrary { [weak self] images in
            //                guard let self = self else { return }
            //                // Use the array of images as needed
            //                for image in images {
            //                    self.imagesArray.append(ImageData(image: image, isChoose: false))
            //                }
            //                self.selectLbl.text = "\(Localizable.localizedString("SelectText")) 1-\(self.imagesArray.count) \(Localizable.localizedString("PhotosOption").lowercased())"
            //                self.collectionView.reloadData()
            //            }
        case .denied:
            break
        @unknown default:
            break
        }
    }
    
    func fetchImagesFromLibrary(completion: @escaping ([UIImage]) -> Void) {
        var images = [UIImage]()
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)] // Sort by creation date, newest first
        
        let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        let imageManager = PHImageManager.default()
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .opportunistic
        
        let targetSize = CGSize(width: 800, height: 900) // Adjust the size based on your requirements
        
        let group = DispatchGroup()
        
        for index in 0..<fetchResult.count {
            imageManager.requestImage(for: fetchResult.object(at: index), targetSize: targetSize, contentMode: .aspectFill, options: requestOptions) { (image, _) in
                if let image = image {
                    images.append(image)
                }
                
                // Check if we've fetched all images
                if index == fetchResult.count - 1{
                    completion(images)
                }
            }
        }
        if images.isEmpty {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.collectionView.reloadData()
                self.removeIndicator()
                self.selectedCollectionView.reloadData()
                self.selectLbl.text = "\(Localizable.localizedString("SelectText")) 1-\(self.fetchResult.count) \(Localizable.localizedString("PhotosOption").lowercased())"
                self.numberLbl.text = "(\(self.arrayNumber.count))"
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func showIndicator() {
        bgIndicator = UIView()
        guard let bgIndicator = bgIndicator else {return}
        bgIndicator.frame = view.bounds
        bgIndicator.backgroundColor = .gray.withAlphaComponent(0.5)
        view.addSubview(bgIndicator)
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        indicator.startAnimating()
        indicator.color = .white
        indicator.style = .large
        bgIndicator.addSubview(indicator)
        indicator.center = bgIndicator.center
    }
    
    func removeIndicator() {
        bgIndicator?.removeFromSuperview()
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
}


extension UIImageView{
    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
        let options = PHImageRequestOptions()
        options.version = .original
        options.deliveryMode = .highQualityFormat
        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { [weak self] image, _ in
            guard let image = image else { return }
            switch contentMode {
            case .aspectFill:
                self?.contentMode = .scaleAspectFill
            case .aspectFit:
                self?.contentMode = .scaleAspectFill
            }
            self?.image = image
        }
    }
}

extension LibraryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension LibraryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1{
            return fetchResult.count
        } else {
            return arrayNumber.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LibraryCollectionViewCell", for: indexPath) as! LibraryCollectionViewCell
            // Thiết lập ảnh trong cell
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            print("indexPath.row\(indexPath.row)")
            let asset = fetchResult.object(at: indexPath.item)
            cell.representedAssetIdentifier = asset.localIdentifier
            imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: options, resultHandler: { [weak self] image, info in
                guard let self = self else { return }
                // UIKit may have recycled this cell by the handler's activation time.
                // Set the cell's thumbnail image only if it's still showing the same asset.
//                if let isInCloud = info?[PHImageRes] as? Bool, isInCloud {
                if let error = info?[PHImageErrorKey] as? Error {
                        // Handle the error
                    if let defaultImage = UIImage(named: "unavailableImage") {
                        self.imageDict[indexPath.row] = ImageData(image: defaultImage, isChoose: false, isAvailable: false)
                    }
                    cell.imageView.image = UIImage(named: "unavailableImage")
                    cell.image = image
                    if let data = imageDict[indexPath.row] {
                        cell.isChoose = data.isChoose
                        cell.isAvailable = false
                    }
                    cell.index = indexPath.row
                    cell.setupCell()
                    cell.delegate = self
                } else {
                    if cell.representedAssetIdentifier == asset.localIdentifier {
                        if imageDict[indexPath.row] == nil {
                            if let imageData = image {
                                self.imageDict[indexPath.row] = ImageData(image: imageData, isChoose: false, isAvailable: true)
                            }
                        }
                        cell.imageView.image = image
                        cell.image = image
                        if let data = imageDict[indexPath.row] {
                            cell.isChoose = data.isChoose
                            cell.isAvailable = true
                        }
                        cell.index = indexPath.row
                        cell.setupCell()
                        cell.delegate = self
                    }
                }
                
            })
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseImageCollectionViewCell", for: indexPath) as! ChooseImageCollectionViewCell
            cell.imageView.image = chooseImage[arrayNumber[indexPath.row]]
            cell.index = indexPath.row
            cell.imageIndex = arrayNumber[indexPath.row]
            cell.delegate = self
            return cell
        }
    }
}

extension LibraryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width: CGFloat = 0
        if collectionView.tag == 1 {
            if UIDevice.current.userInterfaceIdiom == .pad {
                width = (collectionView.frame.width - 21) / 5
            } else {
                width = (collectionView.frame.width - 11) / 3
            }
            thumbnailSize = CGSize(width: 500, height: 600)
            return CGSize(width: width, height: width)
        } else {
            return CGSize(width: 100, height: 100)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}

extension LibraryViewController {
    func showAlert(title:String = "",
                   message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension LibraryViewController: LibraryCollectionCellDelegate {
    func remove(index: Int) {
        print(index)
        chooseImage.removeValue(forKey: index)
        imageDict[index]?.isChoose = false
        for (key, value) in self.arrayNumber.enumerated() {
            if value == index {
                self.arrayNumber.remove(at: key)
            }
        }
        numberLbl.text = "(\(arrayNumber.count))"
        let indexPath = IndexPath(row: index, section: 0)
        if let cell = collectionView.cellForItem(at: indexPath) as? LibraryCollectionViewCell {
            cell.isChoose = false
            cell.setupCell()
        }
//        collectionView.reloadData()
        selectedCollectionView.reloadData()
    }
    
    func select(index: Int, image: UIImage) {
        print(index)
        imageDict[index]?.isChoose = true
        chooseImage[index] = image
        arrayNumber.append(index)
        numberLbl.text = "(\(arrayNumber.count))"
        let indexPath = IndexPath(row: index, section: 0)
        if let cell = collectionView.cellForItem(at: indexPath) as? LibraryCollectionViewCell {
            cell.isChoose = true
            cell.setupCell()
        }
//        collectionView.reloadData()
        selectedCollectionView.reloadData()
    }
    
    func failTapped() {
        let alert = UIAlertController(title: Localizable.localizedString("UnAvailableImageTitle"), message: Localizable.localizedString("UnAvailableImageMessage"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: Localizable.localizedString("LibraryDes"), style: .default, handler: nil)
        alert.addAction(okAction)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        alert.popoverPresentationController?.permittedArrowDirections = []
        present(alert, animated: true)
    }
}

extension LibraryViewController: ChooseImageCollectionDelegate {
    func delete(index: Int, imageIndex: Int) {
        print(imageIndex)
        chooseImage.removeValue(forKey: imageIndex)
        arrayNumber.remove(at: index)
        imageDict[imageIndex]?.isChoose = false
        numberLbl.text = "(\(arrayNumber.count))"
        let indexPath = IndexPath(row: imageIndex, section: 0)
        if let cell = collectionView.cellForItem(at: indexPath) as? LibraryCollectionViewCell {
            cell.isChoose = false
            cell.setupCell()
        }
        selectedCollectionView.reloadData()
    }
    
    
}

extension LibraryViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        if fetchResult == nil {
            return
        }
        guard let changes = changeInstance.changeDetails(for: fetchResult)
        else { return }
        DispatchQueue.main.sync {
            // Hang on to the new fetch result.
            fetchResult = changes.fetchResultAfterChanges
            // If we have incremental changes, animate them in the collection view.
            if changes.hasIncrementalChanges {
                guard let collectionView = self.collectionView else { fatalError() }
                // Handle removals, insertions, and moves in a batch update.
                collectionView.performBatchUpdates({
                    if let removed = changes.removedIndexes, !removed.isEmpty {
                        collectionView.deleteItems(at: removed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let inserted = changes.insertedIndexes, !inserted.isEmpty {
                        collectionView.insertItems(at: inserted.map({ IndexPath(item: $0, section: 0) }))
                    }
                    changes.enumerateMoves { fromIndex, toIndex in
                        collectionView.moveItem(at: IndexPath(item: fromIndex, section: 0),
                                                to: IndexPath(item: toIndex, section: 0))
                    }
                })
                // We are reloading items after the batch update since `PHFetchResultChangeDetails.changedIndexes` refers to
                // items in the *after* state and not the *before* state as expected by `performBatchUpdates(_:completion:)`.
                if let changed = changes.changedIndexes, !changed.isEmpty {
                    collectionView.reloadItems(at: changed.map({ IndexPath(item: $0, section: 0) }))
                }
            } else {
                // Reload the collection view if incremental changes are not available.
                self.collectionView.reloadData()
                self.selectedCollectionView.reloadData()
                self.selectLbl.text = "\(Localizable.localizedString("SelectText")) 1-\(self.imagesArray.count) \(Localizable.localizedString("PhotosOption").lowercased())"
                self.numberLbl.text = "(\(arrayNumber.count))"
            }
            resetCachedAssets()
        }
        
    }
    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }
}

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}
