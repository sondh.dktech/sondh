//
//  PermissionViewController.swift
//  PDFReader
//
//  Created by DEV on 02/12/2023.
//

import UIKit

class PermissionViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var desLbl: UILabel!
    
    @IBOutlet weak var accessBtn: UIButton!
    
    @IBOutlet weak var btnLbl: UILabel!
    
    var isCamera: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        setupView()
        // Do any additional setup after loading the view.
    }

    
    private func setupView() {
        if isCamera {
            titleLbl.text = Localizable.localizedString("CameraPermissionTitle")
            btnLbl.text = Localizable.localizedString("AllowAccessButton")
            desLbl.text = Localizable.localizedString("CameraPermissionDescription")
        } else {
            titleLbl.text = Localizable.localizedString("PhotoPermissionTitle")
            btnLbl.text = Localizable.localizedString("AllowAccessButton")
            desLbl.text = Localizable.localizedString("PhotoPermissionDescription")
        }
        accessBtn.layer.cornerRadius = 10
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func accessTapped(_ sender: Any) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if isCamera {
            UserDefaultsManager.shared.isOpenCamera = true
        } else {
            UserDefaultsManager.shared.isOpenLibrary = true
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
}
