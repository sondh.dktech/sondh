//
//  SettingViewController.swift
//  PDFReader
//
//  Created by DEV on 19/09/2023.
//

import UIKit

class SettingViewController: UIViewController {

    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var items = ["LanguageTitle", "SettingRating", "About"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        view.backgroundColor = UIColor(rgb: 0xF5F5F5)
        setupTableView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupText()
        tableView.reloadData()
    }
    
    func showRating() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id6467587935?action=write-review") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            UserDefaultsManager.shared.isRatingStar = true
            UserDefaultsManager.shared.isRequestRate = true
        }
    }
    
    func setupText() {
        titleLbl.text = Localizable.localizedString("Setting")
        titleLbl.translatesAutoresizingMaskIntoConstraints = false
        if UserDefaultsManager.shared.isiPad() {
            titleLbl.font = UIFont(name: "Inter-Bold", size: 40)
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24).isActive = true
            titleLbl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24).isActive = true
        } else {
            titleLbl.font = UIFont(name: "Inter-Bold", size: 22)
        }
    }
    
    func setupTableView() {
        tableView.backgroundColor = .clear
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "SettingTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingTableViewCell")
    }

}

extension SettingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = ChooseLanguageViewController()
            navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 1 {
            showRating()
        } else if indexPath.row == 2 {
            let vc = AppInfoViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension SettingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UserDefaultsManager.shared.isiPad() {
            return 100
        } else {
            return 64
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell") as! SettingTableViewCell
        cell.selectionStyle = .none
        cell.languageImg.image = UIImage(named: "setting_\(indexPath.row)")
        cell.languageLbl.text = Localizable.localizedString("\(items[indexPath.row])")
        if indexPath.row != 0 {
            cell.language.text = ""
            cell.chooseLanguagBtn.isHidden = true
        } else {
            cell.language.text = Localizable.localizedString("ChooseLanguage")
            cell.chooseLanguagBtn.isHidden = false
        }
        return cell
    }
    
    
}
