//
//  AppInfoViewController.swift
//  PDFReader
//
//  Created by DEV on 20/09/2023.
//

import UIKit

class AppInfoViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var appName: UILabel!
    @IBOutlet weak var appIcon: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        versionLbl.text = "\(Localizable.localizedString("Version"))" + " 1.1.0"
        titleLbl.text = Localizable.localizedString("About")
        if UserDefaultsManager.shared.isiPad() {
            appName.font = UIFont(name: "Inter-Bold", size: 28)
            versionLbl.font = UIFont(name: "Inter", size: 17)
            titleLbl.font = UIFont(name: "Inter-Bold", size: 40)
            appIcon.widthAnchor.constraint(equalToConstant: 132).isActive = true
            appIcon.heightAnchor.constraint(equalToConstant: 132).isActive = true
        } else {
            titleLbl.font = UIFont(name: "Inter-Bold", size: 22)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
