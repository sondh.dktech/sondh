//
//  SettingTableViewCell.swift
//  PDFReader
//
//  Created by DEV on 20/09/2023.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var languageImg: UIImageView!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var chooseLanguagBtn: UIImageView!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        chooseLanguagBtn.isHidden = true
        if UserDefaultsManager.shared.isiPad() {
            bgView.layer.cornerRadius = 15
        } else {
            bgView.layer.cornerRadius = 8
        }
        bgView.layer.masksToBounds = true
        bgView.backgroundColor = .white
        self.backgroundColor = UIColor(rgb: 0xF5F5F5)
        if UserDefaultsManager.shared.isiPad() {
            languageLbl.font = UIFont(name: "Inter", size: 20)
            language.font = UIFont(name: "Inter-Bold", size: 20)
        } else {
            languageLbl.font = UIFont(name: "Inter", size: 16)
            language.font = UIFont(name: "Inter-Bold", size: 14)
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
