//
//  ImportViewController.swift
//  PDFReader
//
//  Created by Pham Van Thai on 16/4/24.
//

enum CurrentChoose {
    case none
    case photo
    case camera
    case file
}

protocol ImportDelegate: AnyObject {
    func chooseType(type: CurrentChoose)
}

import GoogleMobileAds
import UIKit

class ImportViewController: UIViewController {

    @IBOutlet weak var bgNativeAds: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var fileView: UIView!
    @IBOutlet weak var photoLbl: UILabel!
    @IBOutlet weak var cameraLbl: UILabel!
    @IBOutlet weak var fileLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var currentChoose: CurrentChoose = .none
    weak var delegate: ImportDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNativeAds()
    }
    
    func setupView() {
        titleLbl.text = Localizable.localizedString("SourceTitle")
        cancelBtn.setTitle(Localizable.localizedString("RateCancel"), for: .normal)
        photoLbl.text = Localizable.localizedString("PhotosOption")
        cameraLbl.text = Localizable.localizedString("CameraOption")
        fileLbl.text = Localizable.localizedString("FileOption")
        if UserDefaultsManager.shared.isiPad() {
            photoView.layer.cornerRadius = 16
            cameraView.layer.cornerRadius = 16
            fileView.layer.cornerRadius = 16
        } else {
            photoView.layer.cornerRadius = 8
            cameraView.layer.cornerRadius = 8
            fileView.layer.cornerRadius = 8
        }
        photoView.layer.masksToBounds = true
        cameraView.layer.masksToBounds = true
        fileView.layer.masksToBounds = true
        photoView.layer.borderColor = UIColor(rgb: 0x1890FF).cgColor
        cameraView.layer.borderColor = UIColor(rgb: 0x1890FF).cgColor
        fileView.layer.borderColor = UIColor(rgb: 0x1890FF).cgColor
    }
    
    func setupBorder() {
        switch currentChoose {
        case .none:
            photoView.layer.borderWidth = 0
            cameraView.layer.borderWidth = 0
            fileView.layer.borderWidth = 0
        case .photo:
            photoView.layer.borderWidth = 2
            cameraView.layer.borderWidth = 0
            fileView.layer.borderWidth = 0
        case .camera:
            photoView.layer.borderWidth = 0
            cameraView.layer.borderWidth = 2
            fileView.layer.borderWidth = 0
        case .file:
            photoView.layer.borderWidth = 0
            cameraView.layer.borderWidth = 0
            fileView.layer.borderWidth = 2
        }
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true)
        delegate?.chooseType(type: currentChoose)
    }
    
    
    @IBAction func photoTapped(_ sender: Any) {
        currentChoose = .photo
        setupBorder()
        cancelBtn.setTitleColor(UIColor(rgb: 0x1890FF), for: .normal)
        cancelBtn.setTitle(Localizable.localizedString("ConfirmJump"), for: .normal)
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        currentChoose = .camera
        setupBorder()
        cancelBtn.setTitleColor(UIColor(rgb: 0x1890FF), for: .normal)
        cancelBtn.setTitle(Localizable.localizedString("ConfirmJump"), for: .normal)
    }
    
    @IBAction func fileTapped(_ sender: Any) {
        currentChoose = .file
        setupBorder()
        cancelBtn.setTitleColor(UIColor(rgb: 0x1890FF), for: .normal)
        cancelBtn.setTitle(Localizable.localizedString("ConfirmJump"), for: .normal)
    }
    
    
    func setupNativeAds() {
        let idAd = AdMobConstants.NATIVE_SELECT_SOURCE
        let config = NativePlugin.Config(defaultAdUnitId: idAd, defaultAdUnitIdBu: "", defaultNativeType: .NewCustom, borderBGColor: UIColor.black.cgColor, bgColorNativeAds: UIColor(rgb: 0xF5F5F5), colorButton: UIColor.red, loadTestMode: false, textColor: UIColor(rgb: 0x4F646F), buttonColor: UIColor(rgb: 0x1890FF), adColor: UIColor(rgb: 0x1890FF))
            let _ = NativePlugin(rootViewController: self, adContainer: bgNativeAds, config: config)
    }

}


