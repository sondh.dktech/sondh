//
//  MainViewController.swift
//  PDFReader
//
//  Created by DEV on 19/09/2023.
//

import UIKit
import Adjust
import AppTrackingTransparency
import FirebaseAnalytics

enum CurrentTab {
    case home
    case favorite
    case setting
}

class MainViewController: UIViewController, AdjustDelegate {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var homeBtn: UIView!
    @IBOutlet weak var favoriteBtn: UIView!
    @IBOutlet weak var settingBtn: UIView!
    @IBOutlet weak var homeImg: UIImageView!
    @IBOutlet weak var favoriteImg: UIImageView!
    @IBOutlet weak var settingImg: UIImageView!
    @IBOutlet weak var homeLbl: UILabel!
    @IBOutlet weak var favoriteLbl: UILabel!
    @IBOutlet weak var settingLbl: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var bannerView: UIView!
    
    let homevc = HomeViewController()
    let favoritevc = FavoriteViewController()
    let settingvc = SettingViewController()
    var isTab : CurrentTab = .home
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent("home_screen_\(AdMobConstants.VERSION)", parameters: nil)
        navigationController?.setNavigationBarHidden(true, animated: true)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        self.view.backgroundColor = UIColor.white
        stackView.backgroundColor = UIColor.white
        self.overrideUserInterfaceStyle = .light
        UserDefaultsManager.shared.isShowInter = true
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] success, _ in
            if !UserDefaultsManager.shared.isNotiChoose {
                self?.checkNotificationPermission()
                self?.requestATT()
            } else {
                UserDefaultsManager.shared.isShowInter = false
                DispatchQueue.main.async { [weak self] in
                    self?.setupBanner()
                }
            }
            guard success else { return }
            print("Success in APNS registry")
        }
        setupIpadTabBar()
        switchToViewController(homevc)
        setTabbar()
        setupBtn()
        setupText()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupText()
    }
    
    func requestATT() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            ATTrackingManager.requestTrackingAuthorization { [weak self] status in
                switch status {
                case .authorized:
                    UserDefaultsManager.shared.isShowInter = false
                    DispatchQueue.main.async { [weak self] in
                        self?.setupBanner()
                    }
                    print("AppTracking authorization granted")
                    Analytics.logEvent("allowed_ATT_\(AdMobConstants.VERSION)", parameters: nil)
                case .denied:
                    UserDefaultsManager.shared.isShowInter = false
                    DispatchQueue.main.async { [weak self] in
                        self?.setupBanner()
                    }
                    print("AppTracking authorization denied")
                    Analytics.logEvent("denied_ATT_\(AdMobConstants.VERSION)", parameters: nil)
                case .notDetermined:
                    UserDefaultsManager.shared.isShowInter = false
                    DispatchQueue.main.async { [weak self] in
                        self?.setupBanner()
                    }
                    print("AppTracking authorization not determined")
                    Analytics.logEvent("not_determined_ATT_\(AdMobConstants.VERSION)", parameters: nil)
                case .restricted:
                    UserDefaultsManager.shared.isShowInter = false
                    DispatchQueue.main.async { [weak self] in
                        self?.setupBanner()
                    }
                    print("AppTracking authorization restricted")
                    Analytics.logEvent("restricted_ATT_\(AdMobConstants.VERSION)", parameters: nil)
                @unknown default:
                    print("Unknown AppTracking authorization status")
                    Analytics.logEvent("unknown_ATT_\(AdMobConstants.VERSION)", parameters: nil)
                }
                
            }
        }
    }
    
    func checkNotificationPermission() {
        let center = UNUserNotificationCenter.current()
        
        center.getNotificationSettings { [weak self] settings in
            switch settings.authorizationStatus {
            case .authorized:
                UserDefaultsManager.shared.isNotiChoose = true
                self?.requestATT()
                print("Notification permission is granted.")
                Analytics.logEvent("allowed_Noti_\(AdMobConstants.VERSION)", parameters: nil)
            case .denied:
                UserDefaultsManager.shared.isNotiChoose = true
                self?.requestATT()
                print("Notification permission is denied.")
                Analytics.logEvent("denied_Noti_\(AdMobConstants.VERSION)", parameters: nil)
            case .notDetermined:
                print("Notification permission is not determined.")
                self?.requestATT()
                Analytics.logEvent("notDetermined_Noti_\(AdMobConstants.VERSION)", parameters: nil)
            case .provisional:
                UserDefaultsManager.shared.isNotiChoose = true
                self?.requestATT()
                print("Notification permission is provisional.")
                Analytics.logEvent("provisional_Noti_\(AdMobConstants.VERSION)", parameters: nil)
            @unknown default:
                break
            }
        }
    }
    
    
    
    func setupText() {
        homeLbl.text = Localizable.localizedString("Home")
        favoriteLbl.text = Localizable.localizedString("Favorite")
        settingLbl.text = Localizable.localizedString("Setting")
    }
    
    func switchToViewController(_ viewController: UIViewController) {
        containerView.subviews.forEach { $0.removeFromSuperview() }
        addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.didMove(toParent: self)
    }
    
    func setupBtn() {
        let homeGesture = UITapGestureRecognizer(target: self, action: #selector(homeTapped))
        homeBtn.addGestureRecognizer(homeGesture)
        let favorGesture = UITapGestureRecognizer(target: self, action: #selector(favoriteTapped))
        favoriteBtn.addGestureRecognizer(favorGesture)
        let settingGesture = UITapGestureRecognizer(target: self, action: #selector(settingTapped))
        settingBtn.addGestureRecognizer(settingGesture)
    }
    
    
    func setupIpadTabBar() {
        if UserDefaultsManager.shared.isiPad() {
            containerView.removeAllConstraints()
            stackView.removeAllConstraints()
            homeImg.removeAllConstraints()
            favoriteImg.removeAllConstraints()
            settingImg.removeAllConstraints()
            homeLbl.removeAllConstraints()
            favoriteLbl.removeAllConstraints()
            settingLbl.removeAllConstraints()
            containerView.translatesAutoresizingMaskIntoConstraints = false
            stackView.translatesAutoresizingMaskIntoConstraints = false
            homeImg.translatesAutoresizingMaskIntoConstraints = false
            favoriteImg.translatesAutoresizingMaskIntoConstraints = false
            settingImg.translatesAutoresizingMaskIntoConstraints = false
            homeLbl.translatesAutoresizingMaskIntoConstraints = false
            favoriteLbl.translatesAutoresizingMaskIntoConstraints = false
            settingLbl.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                stackView.heightAnchor.constraint(equalToConstant: 80),
                stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                stackView.bottomAnchor.constraint(equalTo: bannerView.topAnchor),
                
                bannerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                bannerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                bannerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),

                
                containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                containerView.bottomAnchor.constraint(equalTo: stackView.topAnchor),
                
                homeImg.centerYAnchor.constraint(equalTo: homeBtn.centerYAnchor),
                homeImg.widthAnchor.constraint(equalToConstant: 32),
                homeImg.heightAnchor.constraint(equalToConstant: 32),
                homeImg.trailingAnchor.constraint(equalTo: homeBtn.centerXAnchor, constant: -10),
                
                homeLbl.centerYAnchor.constraint(equalTo: homeBtn.centerYAnchor),
                homeLbl.leadingAnchor.constraint(equalTo: homeBtn.centerXAnchor, constant: 10),
                
                favoriteImg.centerYAnchor.constraint(equalTo: favoriteBtn.centerYAnchor),
                favoriteImg.widthAnchor.constraint(equalToConstant: 32),
                favoriteImg.heightAnchor.constraint(equalToConstant: 32),
                favoriteImg.trailingAnchor.constraint(equalTo: favoriteBtn.centerXAnchor, constant: -10),
                
                favoriteLbl.centerYAnchor.constraint(equalTo: favoriteBtn.centerYAnchor),
                favoriteLbl.leadingAnchor.constraint(equalTo: favoriteBtn.centerXAnchor, constant: 10),
                
                settingImg.centerYAnchor.constraint(equalTo: settingBtn.centerYAnchor),
                settingImg.widthAnchor.constraint(equalToConstant: 32),
                settingImg.heightAnchor.constraint(equalToConstant: 32),
                settingImg.trailingAnchor.constraint(equalTo: settingBtn.centerXAnchor, constant: -10),
                
                settingLbl.centerYAnchor.constraint(equalTo: settingBtn.centerYAnchor),
                settingLbl.leadingAnchor.constraint(equalTo: settingBtn.centerXAnchor, constant: 10),
            ])
        }
    }
    
    func setupBanner() {
//        if let id = APIManager.shared.adIDs[AdMobConstants.BANNER_RESULT_COLLAPSE] {
//        let id = "ca-app-pub-3940256099942544/2934735716"
        let id = AdMobConstants.BANNER_HOME
            let config = BannerPlugin.Config(defaultAdUnitId: id, defaultBannerType: .Adaptive)
                    let _ = BannerPlugin(rootViewController: self, adContainer: bannerView, config: config)
//        }
    }
    
    func setTabbar() {
        switch isTab {
        case .home:
            homeImg.image = UIImage(named: "home_selected")
            favoriteImg.image = UIImage(named: "favorite_unselected")
            settingImg.image = UIImage(named: "setting_unselected")
            homeLbl.textColor = UIColor(rgb: 0xCC1515)
            favoriteLbl.textColor = UIColor(rgb: 0xA7ADAF)
            settingLbl.textColor = UIColor(rgb: 0xA7ADAF)
        case .favorite:
            homeImg.image = UIImage(named: "home_unselected")
            favoriteImg.image = UIImage(named: "favorite_selected")
            settingImg.image = UIImage(named: "setting_unselected")
            homeLbl.textColor = UIColor(rgb: 0xA7ADAF)
            favoriteLbl.textColor = UIColor(rgb: 0xCC1515)
            settingLbl.textColor = UIColor(rgb: 0xA7ADAF)
        case .setting:
            homeImg.image = UIImage(named: "home_unselected")
            favoriteImg.image = UIImage(named: "favorite_unselected")
            settingImg.image = UIImage(named: "setting_selected")
            homeLbl.textColor = UIColor(rgb: 0xA7ADAF)
            favoriteLbl.textColor = UIColor(rgb: 0xA7ADAF)
            settingLbl.textColor = UIColor(rgb: 0xCC1515)
        }
    }
    
    @objc func homeTapped(_ sender: Any) {
        switchToViewController(homevc)
        isTab = .home
        setTabbar()
    }
    
    @objc func favoriteTapped(_ sender: Any) {
        switchToViewController(favoritevc)
        isTab = .favorite
        setTabbar()
    }
    
    @objc func settingTapped(_ sender: Any) {
        switchToViewController(settingvc)
        isTab = .setting
        setTabbar()
    }
}

