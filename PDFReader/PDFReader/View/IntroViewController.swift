//
//  IntroViewController.swift
//  HairClipper
//
//  Created by DEV on 10/08/2023.
//

import UIKit
import UserNotifications
import FirebaseAnalytics

enum Pages: Int {
    case first = 1
    case second = 2
    case third = 3
}

class IntroViewController: UIViewController {

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstIntroItem: UIImageView!
    @IBOutlet weak var secondIntroItem: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var thirdIntroItem: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bgNativeAds: UIView!
    
    var arrImages: [UIImage] = []
    var nextPage: Pages = .first
    var currentLanguage: Language?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        nextBtn.setTitle(Localizable.localizedString("IntroButtonNext"), for: .normal)
        if UserDefaultsManager.shared.isiPad() {
            nextBtn.titleLabel?.font = UIFont(name: "Inter-Bold", size: 20)
            stackView.heightAnchor.constraint(equalToConstant: 16).isActive = true
            stackView.widthAnchor.constraint(equalToConstant: 60).isActive = true
            nextBtn.widthAnchor.constraint(equalToConstant: 170).isActive = true
            nextBtn.heightAnchor.constraint(equalToConstant: 64).isActive = true
        }
        firstIntroItem.image = UIImage(named: "intro_active")
        loadScrollView()
        setupNativeAds()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nextBtn.layer.masksToBounds = true
        if UserDefaultsManager.shared.isiPad() {
            nextBtn.layer.cornerRadius = 22
        } else {
            nextBtn.layer.cornerRadius = 20
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    func setupNativeAds() {
//        let idAd = "ca-app-pub-3940256099942544/3986624511"
        let idAd = AdMobConstants.NATIVE_INTRO
        let config = NativePlugin.Config(defaultAdUnitId: idAd, defaultAdUnitIdBu: "", defaultNativeType: .Custom, borderBGColor: UIColor.black.cgColor, bgColorNativeAds: UIColor(rgb: 0xF5F5F5), colorButton: UIColor(rgb: 0x1890FF), loadTestMode: false, textColor: UIColor(rgb: 0x4F646F), buttonColor: UIColor(rgb: 0x1890FF), adColor: UIColor(rgb: 0x1890FF))
            let _ = NativePlugin(rootViewController: self, adContainer: bgNativeAds, config: config)
        
    }
    
    func loadScrollView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bgNativeAds.topAnchor, constant: -10)
        ])
        scrollView.delegate = self
        scrollView.backgroundColor = UIColor.clear
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        for i in (0..<3) {
            
            let imageView = UIImageView()
            self.scrollView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                imageView.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor, constant: CGFloat(i)*(UIScreen.main.bounds.size.width)),
                imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100),
                imageView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width),
                imageView.bottomAnchor.constraint(equalTo: bgNativeAds.topAnchor, constant: -10)
                
            ])
            imageView.contentMode = .scaleAspectFit
            imageView.layer.masksToBounds = true
            if UserDefaultsManager.shared.isiPad() {
                imageView.image = UIImage(named: "intro_\(i+1)_ipad")
            } else {
                imageView.image = UIImage(named: "intro_\(i+1)")
            }
            
            let label = UILabel()
            self.scrollView.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
                label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            ])
            if UserDefaultsManager.shared.isiPad() {
                label.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: CGFloat(i)*(UIScreen.main.bounds.size.width) + 100).isActive = true
            } else {
                label.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: CGFloat(i)*(UIScreen.main.bounds.size.width) + 30).isActive = true
            }
            label.textAlignment = .center
            label.numberOfLines = 0
            label.text = Localizable.localizedString("Intro\(i+1)_Des")
            label.textColor = .black
            if UserDefaultsManager.shared.isiPad() {
                label.font = UIFont(name: "Inter-Bold", size: 34)
            } else {
                label.font = UIFont(name: "Inter-Bold", size: 22)
            }
            
        }
        
        let width1 = (Float(3) * Float(UIScreen.main.bounds.size.width))
        scrollView.contentSize = CGSize(width: CGFloat(width1), height: self.view.frame.size.height)
        Analytics.logEvent("intro1_screen_\(AdMobConstants.VERSION)", parameters: nil)
    }


    @IBAction func nextPage(_ sender: Any) {
        setupNextPage()
    }
    
    func setupNextPage() {
        switch nextPage {
        case .first:
            nextPage = .second
            nextBtn.setTitle(Localizable.localizedString("IntroButtonNext"), for: .normal)
            firstIntroItem.image = UIImage(named: "intro_inactive")
            secondIntroItem.image = UIImage(named: "intro_active")
            thirdIntroItem.image = UIImage(named: "intro_inactive")
            self.scrollView.contentOffset.x = self.scrollView.contentOffset.x + self.view.frame.size.width
            Analytics.logEvent("intro2_screen_\(AdMobConstants.VERSION)", parameters: nil)
        case .second:
            nextPage = .third
            nextBtn.setTitle(Localizable.localizedString("IntroButtonStart"), for: .normal)
            firstIntroItem.image = UIImage(named: "intro_inactive")
            secondIntroItem.image = UIImage(named: "intro_inactive")
            thirdIntroItem.image = UIImage(named: "intro_active")
            self.scrollView.contentOffset.x = self.scrollView.contentOffset.x + self.view.frame.size.width
            Analytics.logEvent("intro3_screen_\(AdMobConstants.VERSION)", parameters: nil)
        case .third:
            let vc = MainViewController()
            navigationController?.pushViewController(vc, animated: true)
            navigationController?.setViewControllers([vc], animated: true)
            UserDefaultsManager.shared.isFirstOpenApp = true
            UserDefaultsManager.shared.isFirstOpenChooseLanguage = true
        }
    }
    
    func handleNextPage() {
        switch nextPage {
        case .first:
            nextBtn.setTitle(Localizable.localizedString("IntroButtonNext"), for: .normal)
            firstIntroItem.image = UIImage(named: "intro_active")
            secondIntroItem.image = UIImage(named: "intro_inactive")
            thirdIntroItem.image = UIImage(named: "intro_inactive")
            Analytics.logEvent("intro1_screen_\(AdMobConstants.VERSION)", parameters: nil)
        case .second:
            nextBtn.setTitle(Localizable.localizedString("IntroButtonNext"), for: .normal)
            firstIntroItem.image = UIImage(named: "intro_inactive")
            secondIntroItem.image = UIImage(named: "intro_active")
            thirdIntroItem.image = UIImage(named: "intro_inactive")
            Analytics.logEvent("intro2_screen_\(AdMobConstants.VERSION)", parameters: nil)
        case .third:
            nextBtn.setTitle(Localizable.localizedString("IntroButtonStart"), for: .normal)
            firstIntroItem.image = UIImage(named: "intro_inactive")
            secondIntroItem.image = UIImage(named: "intro_inactive")
            thirdIntroItem.image = UIImage(named: "intro_active")
            Analytics.logEvent("intro3_screen_\(AdMobConstants.VERSION)", parameters: nil)
        }
    }
}

extension IntroViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y != 0 {
                scrollView.contentOffset.y = 0
            }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        
        if pageNumber == 0 && nextPage != .first{
            nextPage = .first
            handleNextPage()
        }else if pageNumber == 1 {
            nextPage = .second
            handleNextPage()
        } else if pageNumber == 2 {
            nextPage = .third
            handleNextPage()
        }
        
    }
}

