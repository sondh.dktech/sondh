//
//  SceneDelegate.swift
//  PDFReader
//
//  Created by DEV on 19/09/2023.
//

import UIKit
import FacebookCore
import FBSDKCoreKit
import PDFKit
import Photos

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var isDidEnterBackground = false
    let notiManager = LocalNotificationManager()
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        let window = UIWindow(frame: UIScreen.main.bounds)
        guard let scene = (scene as? UIWindowScene) else { return }
        window.windowScene = scene
        if UserDefaultsManager.shared.isOpenLibrary {
            UserDefaultsManager.shared.isOpenLibrary = false
            let vc = UINavigationController(rootViewController: MainViewController())
            InterSaveAdsManager.shared.loadInter()
            OnResumeManager.shared.loadAd(loadTestMode: true)
            window.rootViewController = vc
            let accessLevel: PHAccessLevel = .readWrite
            DispatchQueue.main.async { [weak self] in
                PHPhotoLibrary.requestAuthorization(for: accessLevel) { [weak self] status in
                    switch status {
                    case .limited:
                        DispatchQueue.main.async { [weak self] in
                            let libVC = LibraryViewController()
                            vc.pushViewController(libVC, animated: false)
                        }
                    case .authorized:
                        UserDefaultsManager.shared.isFullAccess = true
                        DispatchQueue.main.async { [weak self] in
                            let libVC = LibraryViewController()
                            vc.pushViewController(libVC, animated: false)
                        }
                    case .denied, .restricted:
                        DispatchQueue.main.async { [weak self] in
                            let permissionvc = PermissionViewController()
                            permissionvc.isCamera = false
                            vc.pushViewController(permissionvc, animated: false)
                        }
                    @unknown default:
                        break
                    }
                }
            }
        } else if UserDefaultsManager.shared.isOpenCamera {
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
            UserDefaultsManager.shared.isOpenCamera = false
            let vc = UINavigationController(rootViewController: MainViewController())
            InterSaveAdsManager.shared.loadInter()
            OnResumeManager.shared.loadAd(loadTestMode: true)
            window.rootViewController = vc
            if cameraAuthorizationStatus == .authorized{
                let camVC = CameraViewController()
                vc.pushViewController(camVC, animated: false)
            } else if cameraAuthorizationStatus == .denied || cameraAuthorizationStatus == .restricted{
                DispatchQueue.main.async { [weak self] in
                    let permissionvc = PermissionViewController()
                    permissionvc.isCamera = true
                    vc.pushViewController(permissionvc, animated: false)
                }
            }
        } else {
            let vc = UINavigationController(rootViewController: SplashViewController())
            window.rootViewController = vc
        }
        window.makeKeyAndVisible()
        self.window = window
    }
    
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        print("andsa3")
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        notiManager.removeNoti()
        print("andsa2")
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        UIApplication.shared.applicationIconBadgeNumber = 0
        if isDidEnterBackground {
            isDidEnterBackground = false
            if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
               let rootViewController = windowScene.windows.last?.rootViewController {
                if !UserDefaultsManager.shared.isShowInter && !UserDefaultsManager.shared.isRequestRate && !UserDefaultsManager.shared.isShowSplashInter && !UserDefaultsManager.shared.isSplash && !UserDefaultsManager.shared.isOpenCamera &&  !UserDefaultsManager.shared.isOpenLibrary && !UserDefaultsManager.shared.isShowRatingStar {
                    OnResumeManager.shared.showAdIfAvailable(viewController: rootViewController)
                } else {
                    UserDefaultsManager.shared.isRequestRate = false
                }
            }
            UserDefaultsManager.shared.isOpenCamera = false
            UserDefaultsManager.shared.isOpenLibrary = false
        }
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        print("andsa1")
        if UserDefaultsManager.shared.currentPDF != "" {
            let notiTitle = String(format: "Return to Reading %@", UserDefaultsManager.shared.currentPDF)
            let notiText = String(format: NSLocalizedString("NotiText", comment: ""), UserDefaultsManager.shared.currentPDF, UserDefaultsManager.shared.currentPDF)
            notiManager.notifications = [NotificationAlerts(id: "Alert", title: notiTitle, body: notiText)]
            notiManager.schedule()
        }
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        print("andsa4")
    }
    
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        print("andsa5")
        //true
        isDidEnterBackground = true
        NotificationCenter.default.post(name: NSNotification.Name("DismissKeyboard"), object: nil)
    }
    
    func pdfFileAlreadySaved(url:String, fileName:String)-> Bool {
        var status = false
        if #available(iOS 10.0, *) {
            do {
                let docURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let contents = try FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
                for content in contents {
                    if URL(string: content.description)!.lastPathComponent == fileName {
                        status = true
                    }
                }
            } catch {
                print("could not locate pdf file !!!!!!!")
            }
        }
        return status
    }
    
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "\(fileName)"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                if self.isPDFFile(fileURL: url!) {
                    try pdfData?.write(to: actualPath, options: .atomic)
                }
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
    
    func getPdfFileSize(filePath: String) -> Int64? {
        do {
            // Get the file attributes
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: filePath)
            
            // Extract the file size
            if let fileSize = fileAttributes[FileAttributeKey.size] as? Int64 {
                return fileSize
            }
        } catch {
            // Handle any errors that occur during the process
            print("Error: \(error)")
        }
        
        // Return nil if the file size couldn't be retrieved
        return nil
    }
    
    func bytesToMegabytes(bytes: Int64) -> Double {
        let megabyte = 1024 * 1024 // 1 MB = 1024 KB = 1024 * 1024 bytes
        return Double(bytes) / Double(megabyte)
    }
    
    func isPDFFile(fileURL: URL) -> Bool {
        // Get the file's path extension (e.g., "pdf" from "example.pdf")
//        let fileExtension = fileURL.pathExtension.lowercased()
//        
//        // Check if the file extension is "pdf"
//        return fileExtension == "pdf"
        do {
            let pdfData = try Data(contentsOf: fileURL)
            // Use the pdfData as needed
            if pdfData.isPDF {
                return true
            } else {
                return false
            }
        } catch {
            print("Error loading PDF data: \(error.localizedDescription)")
            return false
        }
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }
        var data = UserDefaultsManager.shared.getListPDF()
        let window = UIWindow(frame: UIScreen.main.bounds)
        guard let scene = (scene as? UIWindowScene) else { return }
        window.windowScene = scene
        let vc = UINavigationController(rootViewController: MainViewController())
        window.rootViewController = vc
        if self.isPDFFile(fileURL: url) {
            let pdfFileName = url.lastPathComponent
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let appUrl = documentsDirectory.appendingPathComponent(pdfFileName)
                if !self.pdfFileAlreadySaved(url: appUrl.absoluteString, fileName: pdfFileName) {
                    self.savePdf(urlString: url.absoluteString, fileName: pdfFileName)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    let currentDate = Date()
                    let formattedDate = dateFormatter.string(from: currentDate)
                    let fileSize = self.getPdfFileSize(filePath: url.path)
                    let formatSize = self.bytesToMegabytes(bytes: fileSize!)
                    if self.isPDFFile(fileURL: url) {
                        data.insert(PDFData(url: appUrl.absoluteString, name: pdfFileName, date: formattedDate, size: "\(String(format: "%.2f", formatSize)) MB", isFavorite: false), at: 0)
                    }
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    let currentDate = Date()
                    let formattedDate = dateFormatter.string(from: currentDate)
                    let fileSize = self.getPdfFileSize(filePath: url.path)
                    let formatSize = self.bytesToMegabytes(bytes: fileSize!)
                    let pdfData = PDFData(url: appUrl.absoluteString, name: pdfFileName, date: formattedDate, size: "\(String(format: "%.2f", formatSize)) MB", isFavorite: false)
                    if !data.contains(where: { $0.url == appUrl.absoluteString }) {
                        data.insert(pdfData, at: 0)
                    }
                }
            }
            UserDefaultsManager.shared.savePDF(listPDF: data)
            if let pdfDocument = PDFDocument(url: url) {
                let pdfVC = PDFViewController()
                pdfVC.pdfURL = url.absoluteString
                pdfVC.pdfName = url.lastPathComponent
                pdfVC.pdfDocument = pdfDocument
                vc.pushViewController(pdfVC, animated: true)
            }
            window.makeKeyAndVisible()
            self.window = window
        }
        
        ApplicationDelegate.shared.application(
            UIApplication.shared,
            open: url,
            sourceApplication: nil,
            annotation: [UIApplication.OpenURLOptionsKey.annotation]
        )
    }
    
}

