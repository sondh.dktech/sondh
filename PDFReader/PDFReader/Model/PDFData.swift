//
//  PDFData.swift
//  PDFReader
//
//  Created by DEV on 22/09/2023.
//

import Foundation

struct PDFData: Codable {
    var url: String
    var name: String
    var date: String
    var size: String
    var isFavorite: Bool
}
