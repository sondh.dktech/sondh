//
//  Language.swift
//  Wallpaper
//
//  Created by DEV on 25/07/2023.
//

import Foundation

enum Language: Codable, Equatable {
    case english
    case hindi
    case spanish
    case french
    case arabic
    case russian
    case portuguese
    case indonesian
    case german
    case italian
    case korean
}

extension Language {
    var code: String {
        switch self {
        case .english:          return "en"
        case .hindi:            return "hi"
        case .spanish:          return "es"
        case .french:           return "fr"
        case .arabic:           return "ar"
        case .russian:          return "ru"
        case .portuguese:       return "pt-PT"
        case .indonesian:       return "id"
        case .german:           return "de"
        case .italian:          return "it"
        case .korean:           return "ko"
        }
    }
    
    var name: String {
        switch self {
        case .english:          return "English"
        case .hindi:            return "Hindi"
        case .spanish:          return "Spanish"
        case .french:           return "French"
        case .arabic:           return "Arabic"
        case .russian:          return "Russian"
        case .portuguese:       return "Portuguese"
        case .indonesian:       return "Indonesian"
        case .german:           return "German"
        case .italian:          return "Italian"
        case .korean:           return "Korean"
        }
    }
    
    var label: String {
        switch self {
        case .english:          return "Language"
        case .hindi:            return "भाषा"
        case .spanish:          return "Idioma"
        case .french:           return "Langue"
        case .arabic:           return "اللغة"
        case .russian:          return "Язык"
        case .portuguese:       return "Idioma"
        case .indonesian:       return "Bahasa"
        case .german:           return "Sprache"
        case .italian:          return "Lingua"
        case .korean:           return "언어"
        }
    }
    
    var image: String {
        switch self {
        case .english:          return "ic_english"
        case .hindi:            return "ic_hindi"
        case .spanish:          return "ic_spanish"
        case .french:           return "ic_french"
        case .arabic:           return "ic_arabic"
        case .russian:          return "ic_russian"
        case .portuguese:       return "ic_portuguese"
        case .indonesian:       return "ic_indonesian"
        case .german:           return "ic_german"
        case .italian:          return "ic_italian"
        case .korean:           return "ic_korean"
        }
    }
}

extension Language {
    init?(languageCode: String?) {
        guard let languageCode = languageCode else { return nil }
        switch languageCode {
        case "en":              self = .english
        case "hi":              self = .hindi
        case "es":              self = .spanish
        case "fr":              self = .french
        case "ar":              self = .arabic
        case "ru":              self = .russian
        case "pt-PT":           self = .portuguese
        case "id":              self = .indonesian
        case "it":              self = .italian
        case "de":              self = .german
        case "kr":              self = .korean
        default:                return nil
        }
    }
}
