//
//  Languages.swift
//  Wallpaper
//
//  Created by DEV on 25/07/2023.
//

import Foundation

struct Languages: Codable {
    let language: Language
    var code: Bool
}
