//
//  ImageData.swift
//  PDFReader
//
//  Created by DEV on 05/12/2023.
//

import Foundation
import UIKit

struct ImageData {
    var image: UIImage
    var isChoose: Bool
    var isAvailable: Bool
}

struct ImageConvert {
    var image: UIImage
    var isChoose: Bool
    var isHiddenNumber: Bool
}
