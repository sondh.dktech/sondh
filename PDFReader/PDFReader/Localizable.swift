//
//  Localizable.swift
//  Wallpaper
//
//  Created by DEV on 26/07/2023.
//

import Foundation

class Localizable {
    class func localizedString(_ key: String, comment: String = "") -> String {
        return NSLocalizedString(key, comment: comment)
    }
}
